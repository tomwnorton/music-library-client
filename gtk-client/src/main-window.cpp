//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <functional>
#include <iomanip>
#include <string>
#include <vector>
#include <gtkmm.h>
#include "core/unicode-string.hpp"
#include "core/scsi.hpp"
#include "gtk-client/main-window.hpp"
#include "core/scsi.hpp"

using music_library::core::scsi_service;
using Glib::ustring;

namespace music_library::gtk {
    namespace {
        Glib::RefPtr<Gtk::ListStore> create_sources_model(const scsi_service& scsiService,
                                                          const main_window::SourcesModelColumns& columns) {
            auto sourcesTreeModel = Gtk::ListStore::create(columns);
            auto cdDrives = scsiService.get_cd_drives();
            for (auto& cdDriveInfo : cdDrives) {
                auto row = *(sourcesTreeModel->append());
                row[columns.drive] = cdDriveInfo.get_drive().to_utf8_string().c_str();
                row[columns.vendor] = cdDriveInfo.get_vendor().to_utf8_string().c_str();
                row[columns.productNumber] = cdDriveInfo.get_product_number().to_utf8_string().c_str();
            }
            return sourcesTreeModel;
        }

        Glib::RefPtr<Gtk::ListStore> create_tracks_model(const scsi_service& scsiService,
                                                         const main_window::TracksModelColumns& columns) {
            auto tracksTreeModel = Gtk::ListStore::create(columns);
            auto tracks = scsiService.get_tracks("/dev/sr0").tracks;
            for (auto i = 0UL; i < tracks.size(); ++i) {
                auto row = *(tracksTreeModel->append());
                row[columns.number] = i + 1;
                row[columns.name] = tracks[i].text.title.to_utf8_string();
                row[columns.composer] = tracks[i].text.composer.to_utf8_string();
                row[columns.length] = ustring::compose("%1:%2.%3",
                        ustring::format(std::setfill(L'0'), std::setw(2), tracks[i].length.get_minutes()),
                        ustring::format(std::setfill(L'0'), std::setw(2), tracks[i].length.get_seconds()),
                        ustring::format(std::setfill(L'0'), std::setw(2), tracks[i].length.get_frames()));
            }
            return  tracksTreeModel;
        }
    }

    main_window::main_window() : horizontalBox(Gtk::ORIENTATION_HORIZONTAL) {
        set_default_size(1280, 720);
        set_title("Music Library");
        add(horizontalBox);
        horizontalBox.pack_start(sourcesTreeView);
        horizontalBox.pack_start(tracksTreeView);

        music_library::core::scsi_service scsiService;

        sourcesTreeModel = create_sources_model(scsiService, sourcesColumns);
        sourcesTreeView.set_model(sourcesTreeModel);
        sourcesTreeView.append_column("Drive", sourcesColumns.drive);
        sourcesTreeView.append_column("Vendor", sourcesColumns.vendor);
        sourcesTreeView.append_column("Product Number", sourcesColumns.productNumber);

        tracksTreeModel = create_tracks_model(scsiService, tracksColumns);
        tracksTreeView.set_model(tracksTreeModel);
        tracksTreeView.append_column("Number", tracksColumns.number);
        tracksTreeView.append_column("Title", tracksColumns.name);
        tracksTreeView.append_column("Composer", tracksColumns.composer);
        tracksTreeView.append_column("Length", tracksColumns.length);

        show_all_children();
    }
}