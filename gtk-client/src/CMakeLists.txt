#   Client for my music-library web-app
#   Copyright (C) 2019  Thomas Norton
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTKMM3 REQUIRED gtkmm-3.0)
# Some implementations of FindPkgConfig implement a target, but others don't
if (NOT TARGET PkgConfig::GTKMM3)
    add_library(PkgConfig::GTKMM3 INTERFACE IMPORTED)
    if (GTKMM3_INCLUDE_DIRS)
        set_property(TARGET PkgConfig::GTKMM3 PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${GTKMM3_INCLUDE_DIRS})
    endif()
    if (GTKMM3_LIBRARY_DIRS)
        set_property(TARGET PkgConfig::GTKMM3 PROPERTY INTERFACE_LINK_DIRECTORIES ${GTKMM3_LIBRARY_DIRS})
    endif()
    if (GTKMM3_LINK_LIBRARIES)
        set_property(TARGET PkgConfig::GTKMM3 PROPERTY INTERFACE_LINK_LIBRARIES ${GTKMM3_LINK_LIBRARIES})
    endif()
    if (GTKMM3_CFLAGS_OTHER)
        set_property(TARGET PkgConfig::GTKMM3 PROPERTY INTERFACE_COMPILE_OPTIONS ${GTKMM3_CFLAGS_OTHER})
    endif()
endif()

add_executable(music-library-client main.cpp main-window.cpp)
target_link_libraries(music-library-client PUBLIC PkgConfig::GTKMM3 music-library-client-core music-library-client-headers)