//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_MAIN_WINDOW_HPP
#define MUSIC_LIBRARY_CLIENT_MAIN_WINDOW_HPP

namespace music_library::gtk {
    class main_window : public Gtk::Window {
    public:
        main_window();

        class SourcesModelColumns : public Gtk::TreeModel::ColumnRecord {
        public:
            SourcesModelColumns() {
                add(drive);
                add(vendor);
                add(productNumber);
            }

            Gtk::TreeModelColumn<Glib::ustring> drive;
            Gtk::TreeModelColumn<Glib::ustring> vendor;
            Gtk::TreeModelColumn<Glib::ustring> productNumber;
        };

        class TracksModelColumns : public Gtk::TreeModel::ColumnRecord {
        public:
            TracksModelColumns() {
                add(number);
                add(name);
                add(composer);
                add(length);
            }

            Gtk::TreeModelColumn<int> number;
            Gtk::TreeModelColumn<Glib::ustring> name;
            Gtk::TreeModelColumn<Glib::ustring> composer;
            Gtk::TreeModelColumn<Glib::ustring> length;
        };

    private:
        Gtk::Box horizontalBox;
        Gtk::TreeView sourcesTreeView;
        Gtk::TreeView tracksTreeView;
        Glib::RefPtr<Gtk::ListStore> sourcesTreeModel;
        Glib::RefPtr<Gtk::ListStore> tracksTreeModel;
        SourcesModelColumns sourcesColumns;
        TracksModelColumns tracksColumns;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_MAIN_WINDOW_HPP
