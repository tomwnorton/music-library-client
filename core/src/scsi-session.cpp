//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include <sstream>

#include "core/unicode-string.hpp"
#include "core/exception.hpp"
#include "core/scsi-session.hpp"

namespace music_library::core {
    using namespace unicode_string_literals;

    io_control_handle* open_handle(const unicode_string& devicePath);
    void close_handle(io_control_handle* handle);

    scsi_session::scsi_session(const unicode_string& devicePath) {
        m_handle = open_handle(devicePath);
    }

    scsi_session::scsi_session(music_library::core::scsi_session&& rhs) noexcept {
        m_handle = rhs.m_handle;
        rhs.m_handle = 0;
    }

    scsi_session& scsi_session::operator=(music_library::core::scsi_session&& rhs) noexcept {
        if (this == &rhs) {
            return *this;
        }
        m_handle = rhs.m_handle;
        rhs.m_handle = 0;
        return *this;
    }

    void run_scsi_command(io_control_handle* handle, const unsigned char* request, unsigned char requestSize, void* responseBuffer, unsigned int responseBufferSize);

    void scsi_session::run_command(const unsigned char* request, unsigned char requestSize, void* responseBuffer, unsigned int responseBufferSize) const {
        run_scsi_command(m_handle, request, requestSize, responseBuffer, responseBufferSize);
    }

    scsi_session::~scsi_session() {
        if (m_handle != nullptr) {
            close_handle(m_handle);
        }
    }
}