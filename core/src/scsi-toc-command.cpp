//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <cstring>

#include <string>
#include <vector>

#include "core/unicode-string.hpp"
#include "core/big-endian.hpp"
#include "core/scsi-session.hpp"
#include "core/scsi-command.hpp"
#include "core/scsi.hpp"
#include "core/scsi-metadata-request.hpp"

namespace music_library::core {
    namespace {
        struct minute_second_frame {
            uint8_t reserved;
            uint8_t minutes;
            uint8_t seconds;
            uint8_t frames;
        };

        struct read_toc_response_track_descriptor {
            uint8_t first_reserved_block;
            uint8_t control : 4;
            uint8_t adr : 4;
            uint8_t track_number;
            uint8_t reserved;
            union {
                minute_second_frame msf;
                big_endian<4UL> logical_block_address;
            };
        };

        struct read_toc_response {
            big_endian<2UL> data_length;
            uint8_t first_track_number;
            uint8_t last_track_number;
            read_toc_response_track_descriptor track_data[127];
        };
    }
    typedef scsi_command<0x43, read_toc_pma_atip_request, read_toc_response> table_of_contents_command;

    std::vector<track_info> get_table_of_contents(const scsi_session& session) {
        std::vector<track_info> tracks;
        table_of_contents_command command;
        command.request().track_number = 1;
        command.request().allocation_length = sizeof(read_toc_response);
        command.execute(session, [&](const read_toc_response& response) {
            std::uint32_t previousTrackAddress;
            for (auto i = response.first_track_number - 1; i < response.last_track_number; ++i) {
                auto& currentTrack = response.track_data[i];
                if (i > 0) {
                    track_length length(currentTrack.logical_block_address - previousTrackAddress);
                    track_info trackInfo = {text_info(), length, previousTrackAddress};
                    tracks.push_back(trackInfo);
                }
                previousTrackAddress = currentTrack.logical_block_address;
            }

            table_of_contents_command leadOutCommand;
            leadOutCommand.request().track_number = 0xAA;
            leadOutCommand.request().allocation_length = sizeof(read_toc_response);
            leadOutCommand.execute(session, [&](const read_toc_response& leadOutResponse) {
                track_length length(leadOutResponse.track_data[0].logical_block_address - previousTrackAddress);
                track_info trackInfo = {text_info(), length, previousTrackAddress};
                tracks.push_back(trackInfo);
            });
        });
        return tracks;
    }
}