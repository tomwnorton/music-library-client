//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <cstring>

#include <string>
#include <vector>

#include "core/unicode-string.hpp"
#include "core/big-endian.hpp"
#include "core/scsi-session.hpp"
#include "core/scsi-command.hpp"
#include "core/scsi.hpp"

namespace music_library::core {
    namespace {
        const uint8_t OPERATION_CODE = 0x12;
        const uint8_t PERIPHERAL_QUALIFIER_ATTACHED_DEVICE = 0;
        const uint8_t PERIPHERAL_DEVICE_TYPE_CD_DVD = 5;

        struct inquiry_request {
            uint8_t first_reserved_block;
            uint8_t operation_code;
            bool enable_vital_product_data : 1;
            bool obsolete_formerly_cmddt : 1;
            unsigned reserved : 6;
            std::uint8_t page_code;
            big_endian<2UL> allocation_length;
            std::uint8_t control;
        };

        struct inquiry_standard_data {
            // Byte 0
            unsigned peripheral_device_type : 5;
            unsigned peripheral_qualifier : 3;
            // Byte 1
            unsigned first_reserved_block : 7;
            bool removable_media : 1;
            // Byte 2
            std::uint8_t version;
            // Byte 3
            unsigned response_data_format : 4;
            bool hierarchical_support : 1;
            bool normal_aca_support : 1;
            unsigned first_obsolete_block : 2;
            // Byte 4
            std::uint8_t additional_length;
            // Byte 5
            bool protect : 1;
            unsigned second_reserved_block: 2;
            bool _3pc : 1;
            unsigned tpgs : 2;
            bool acc : 1;
            bool sccs : 1;
            // Byte 6
            unsigned second_obsolete_block : 4;
            bool multip : 1;
            bool vs : 1;
            bool encserv : 1;
            unsigned third_oboslete_block : 1;
            // Byte 7
            bool other_vs : 1;
            bool cmdque : 1;
            unsigned eighth_byte : 6;
            // Bytes 8 - 15
            char vendor_identification[8];
            // Bytes 16 - 31
            char product_identification[16];
            // Bytes 32 - 35
            char product_revision_level[4];
            // Bytes 36 - 43
            char drive_serial_number[8];
            // Bytes 44 - 55
            char vendor_unique[12];
            // Bytes 56 - 57
            std::uint16_t third_reserved_block;
            // Bytes 58 - 73
            std::uint16_t version_descriptors[8];
            // Misc data
            unsigned char fourth_reserved_block[22];
        };
    }

    template<>
    struct scsi_request_traits<inquiry_request> {
        static const size_t OFFSET = 1UL;
    };
    typedef scsi_command<OPERATION_CODE, inquiry_request, inquiry_standard_data> scsi_inquiry_command;

    std::vector<cd_drive_info> scsi_service::get_cd_drives() const {
        char device_path[] = "/dev/sr_";
        std::vector<cd_drive_info> results;
        for (char c = '0'; c <= '9'; ++c) {
            device_path[7] = c;
            try {
                scsi_session session(device_path);
                scsi_inquiry_command command;
                command.request().allocation_length = sizeof(inquiry_standard_data);
                command.execute(session, [&](const inquiry_standard_data& response) {
                    if (response.peripheral_qualifier == PERIPHERAL_QUALIFIER_ATTACHED_DEVICE
                        && response.peripheral_device_type == PERIPHERAL_DEVICE_TYPE_CD_DVD
                        && response.removable_media) {
                        results.emplace_back(cd_drive_info(device_path,
                                                           unicode_string(response.vendor_identification, 8UL),
                                                           unicode_string(response.product_identification, 16UL)));
                    }
                });
            } catch (...) {
            }
        }
        return results;
    }
}