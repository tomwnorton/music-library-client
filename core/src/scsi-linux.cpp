//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include <sstream>

#include <fcntl.h>
#include <unistd.h>
#include <scsi/sg.h>
#include <sys/ioctl.h>

#include "core/unicode-string.hpp"
#include "core/exception.hpp"
#include "core/scsi-session.hpp"

namespace music_library::core {
    using namespace unicode_string_literals;

    struct io_control_handle {
        int fd;
    };

    io_control_handle* open_handle(const unicode_string& devicePath) {
        auto utf8String = devicePath.to_utf8_string();
        auto fd = open(utf8String.c_str(), O_RDWR | O_NONBLOCK);
        if (fd == 0) {
            throw exception("Could not open SCSI device: "_us + devicePath);
        }
        auto handle = new io_control_handle;
        handle->fd = fd;
        return handle;
    }

    void close_handle(io_control_handle* handle) {
        close(handle->fd);
        delete handle;
    }

    void run_scsi_command(io_control_handle* handle, const unsigned char* request, unsigned char requestSize, void* responseBuffer, unsigned int responseBufferSize) {
        unsigned char sense[SG_MAX_SENSE];
        std::memset(sense, 0, SG_MAX_SENSE);
        sg_io_hdr hdr = {0};
        hdr.flags = SG_FLAG_DIRECT_IO;
        hdr.cmdp = const_cast<unsigned char*>(request);
        hdr.cmd_len = requestSize;
        hdr.sbp = sense;
        hdr.mx_sb_len = SG_MAX_SENSE;
        hdr.interface_id = 'S';
        hdr.timeout = 120'000;
        hdr.dxferp = responseBuffer;
        hdr.dxfer_len = responseBufferSize;
        hdr.dxfer_direction = SG_DXFER_TO_FROM_DEV;

        int scsiCommandResult = ioctl(handle->fd, SG_IO, &hdr);

        if (scsiCommandResult != 0 || hdr.status != 0) {
            auto errorCode = errno;
            switch (errorCode) {
                case EBADF:
                    throw exception("Attempted SCSI operation with invalid file descriptor");
                    break;
                case EFAULT:
                    throw exception("Attempted SCSI operation with inaccessible memory");
                    break;
                case EINVAL:
                    throw exception("Attempted SCSI operation with invalid request");
                    break;
                case ENOTTY:
                    throw exception("Attempted SCSI operation which does not apply to the given file descriptor");
                    break;
                default:
                    std::ostringstream out;
                    out << "Attempted SCSI operation which resulted in an unknown error: "
                        << errorCode;
                    throw exception(out.str().c_str());
            }
        }
    }
}