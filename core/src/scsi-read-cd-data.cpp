//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include <cstdint>

#include <algorithm>
#include <string>
#include <vector>
#include <memory>

#include "core/unicode-string.hpp"
#include "core/scsi.hpp"
#include "core/big-endian.hpp"
#include "core/scsi-session.hpp"
#include "core/scsi-command.hpp"
#include "core/scsi-read-cd-data.hpp"

using music_library::core::unicode_string;
using namespace music_library::core::unicode_string_literals;

namespace music_library::core {
    namespace {
        struct read_cd_request {
            uint16_t offset_block;
            uint8_t operation_code;
            // Byte 1
            bool reladr : 1;
            uint8_t first_reserved_block : 1;
            uint8_t sector_type : 3;
            uint8_t second_reserved_block : 3;

            // Bytes [2, 5]
            big_endian<4UL> starting_logical_block_address;

            // Bytes [6, 8]
            big_endian<3UL> transfer_length_in_blocks;

            // Byte 9
            uint8_t third_reserved_block : 1;
            uint8_t error_field : 2;
            bool edc_or_ecc : 1;
            bool user_data : 1;
            uint8_t header_codes : 2;
            bool sync : 1;

            // Byte 10
            uint8_t subchannel_selection_bits : 3;
            uint8_t fourth_reserved_block : 5;

            // Byte 11
            uint8_t control;
        };

        typedef scsi_command<0xBE, read_cd_request, uint8_t> read_cd_data_command;

        std::vector<uint8_t> read_next_sector(const scsi_session& session, int startSector, int numberOfSectors) {
            int bytesToRead = numberOfSectors * BYTES_PER_SECTOR;
            read_cd_data_command command;
            command.request().starting_logical_block_address = startSector;
            command.request().transfer_length_in_blocks = numberOfSectors;
            command.request().sync = true;
            command.request().header_codes = 0;
            command.request().user_data = true;
            command.request().edc_or_ecc = false;

            std::vector<uint8_t> buffer(bytesToRead);
            command.execute(session,
                            [&](const uint8_t& response) {
                                const uint8_t* data = &response;
                                std::copy(data, data + bytesToRead, buffer.begin());
                            },
                            bytesToRead);
            return buffer;
        }
    }

    template<>
    struct scsi_request_traits<read_cd_request> {
        static const size_t OFFSET = 2UL;
    };

    struct scsi_sector_reader_factory_impl {
        scsi_session session;

        scsi_sector_reader_factory_impl(const unicode_string& drivePath) : session(drivePath) {}
    };

    struct scsi_sector_reader_impl {
        std::vector<std::uint8_t> sector;
        scsi_session& session;
        int currentSector;
        int endSector;

        scsi_sector_reader_impl(scsi_session& session, int currentSector, int endSector) : session(session), currentSector(currentSector), endSector(endSector) {}
    };

    scsi_sector_reader_factory::scsi_sector_reader_factory(const music_library::core::unicode_string& drivePath) {
        m_impl = new scsi_sector_reader_factory_impl(drivePath);
    }

    scsi_sector_reader_factory::~scsi_sector_reader_factory() {
        delete m_impl;
    }

    scsi_sector_reader_factory::scsi_sector_reader_factory(music_library::core::scsi_sector_reader_factory&& rhs) {
        m_impl = rhs.m_impl;
        rhs.m_impl = nullptr;
    }

    scsi_sector_reader_factory& scsi_sector_reader_factory::operator=(music_library::core::scsi_sector_reader_factory&& rhs) {
        if (this == &rhs) return *this;

        delete m_impl;
        m_impl = rhs.m_impl;
        rhs.m_impl = nullptr;

        return *this;
    }

    std::unique_ptr<scsi_sector_reader> scsi_sector_reader_factory::create_reader(int startSector, int endSector) {
        return std::unique_ptr<scsi_sector_reader>(new scsi_sector_reader(new scsi_sector_reader_impl(m_impl->session, startSector, endSector)));
    }

    scsi_sector_reader::scsi_sector_reader(music_library::core::scsi_sector_reader_impl* impl) {
        m_impl = impl;
        m_impl->sector = read_next_sector(m_impl->session, m_impl->currentSector, 1);
    }

    scsi_sector_reader::~scsi_sector_reader() {
        delete m_impl;
    }

    bool scsi_sector_reader::empty() const {
        return m_impl->currentSector >= m_impl->endSector;
    }

    void scsi_sector_reader::pop_front() {
        m_impl->sector = read_next_sector(m_impl->session, m_impl->currentSector + 1, 1);
        ++m_impl->currentSector;
    }

    const std::vector<std::uint8_t>& scsi_sector_reader::front() const {
        return m_impl->sector;
    }
}