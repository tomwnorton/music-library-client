//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <cstring>
#include <vector>
#include <sstream>
#include <iomanip>

#include <windows.h>
#include <ntddscsi.h>

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

#include "core/unicode-string.hpp"
#include "core/exception.hpp"


namespace music_library::core {
    using namespace unicode_string_literals;

    namespace {
        const int SENSE_SIZE = 32;
        const int RESPONSE_OFFSET = SENSE_SIZE;

        void throw_exception_from_windows_error(const unicode_string& errorPrefix) {
            auto errorCode = GetLastError();
            WCHAR messageBuffer[4096];
            if (!FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, errorCode, 0, messageBuffer, 4096, nullptr)) {
                std::wostringstream out;
                out << L"Got error " << errorCode << L"; also known as 0x" << std::hex << errorCode;
                throw exception(reinterpret_cast<const char16_t*>(out.str().c_str()));
            }
            std::wostringstream out;
            out << L"Error Code " << errorCode << L": " << messageBuffer;
            throw exception(errorPrefix + reinterpret_cast<const char16_t*>(out.str().c_str()));
        }
    }

    struct io_control_handle {
        HANDLE handle;
    };

    io_control_handle* open_handle(const unicode_string& devicePath) {
        auto handleStruct = new io_control_handle;
        auto cFilePath = devicePath.to_utf16_string().c_str();
        handleStruct->handle = CreateFile(reinterpret_cast<LPCWSTR>(cFilePath), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
        if (handleStruct->handle == INVALID_HANDLE_VALUE) {
            throw_exception_from_windows_error("Could not open file ("_us + devicePath + "): ");
        }
        return handleStruct;
    }

    void close_handle(io_control_handle* handleStruct) {
        CloseHandle(handleStruct->handle);
        delete handleStruct;
    }

    void run_scsi_command(io_control_handle* handle, const unsigned char* request, unsigned char requestSize, void* responseBuffer, unsigned int responseBufferSize) {
        std::vector<uint8_t> buffer(sizeof(SCSI_PASS_THROUGH) + SENSE_SIZE + responseBufferSize, 0);

        static_assert(sizeof(SCSI_PASS_THROUGH) == 56);
        static_assert(offsetof(SCSI_PASS_THROUGH, Cdb) == 36);
        SCSI_PASS_THROUGH scsiPassThrough = { 0 };
        scsiPassThrough.Length = sizeof(SCSI_PASS_THROUGH);
        scsiPassThrough.DataIn = SCSI_IOCTL_DATA_IN;
        scsiPassThrough.TimeOutValue = 120'000;
        scsiPassThrough.SenseInfoLength = SENSE_SIZE;
        scsiPassThrough.DataBufferOffset = RESPONSE_OFFSET;
        scsiPassThrough.DataTransferLength = responseBufferSize;
        scsiPassThrough.CdbLength = requestSize;
        std::memcpy(scsiPassThrough.Cdb, request, requestSize);
        std::memcpy(buffer.data(), &scsiPassThrough, sizeof(SCSI_PASS_THROUGH));

        DWORD bytesReturned;
        auto status = DeviceIoControl(handle->handle,
                IOCTL_SCSI_PASS_THROUGH,
                buffer.data(),
                buffer.size(),
                buffer.data(),
                buffer.size(),
                &bytesReturned,
                nullptr);
        if (!status) {
            throw_exception_from_windows_error("Could not run SCSI command: ");
        }

        std::memcpy(responseBuffer, buffer.data() + RESPONSE_OFFSET, responseBufferSize);
    }
}