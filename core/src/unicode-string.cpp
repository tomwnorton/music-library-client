//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>
#include "core/unicode-string.hpp"

namespace music_library::core {
    namespace {
        const int INVALID_UNICODE_CODEPOINT = 0xFFFF;

        template<typename T>
        std::size_t calculate_size_in_elements(const T* value) {
            std::size_t length = 0UL;
            const T* current = value;
            while (*(current++) != 0) {
                ++length;
            }
            return length;
        }

        // region UTF-8 Logic

        std::size_t calculate_size_in_utf8_characters(const unsigned char* utf8_string) {
            std::size_t length = 0UL;
            while (*utf8_string != 0) {
                if ((*utf8_string & 0xC0U) != 0x80U) {
                    ++length;
                }
                ++utf8_string;
            }
            return length;
        }

        constexpr unsigned int utf8_header_mask(unsigned int number, unsigned int currentMask) {
            if (number == 0U) {
                return currentMask;
            }
            return utf8_header_mask(number - 1U, (currentMask >> 1U) | 0x80U);
        }

        constexpr unsigned int utf8_header_mask(unsigned int number) {
            return utf8_header_mask(number, 0U);
        }

        constexpr unsigned int utf8_header_data_mask(unsigned int number) {
            return ~(utf8_header_mask(number) | 0xFF'FF'FF'00U);
        }

        inline bool is_utf8_header_byte_for_index(unsigned char byte, unsigned int index) {
            return (byte & utf8_header_mask(index + 1)) == utf8_header_mask(index);
        }

        template<unsigned int LENGTH>
        inline char32_t transform_from_utf8_header_byte(unsigned char byte) {
            return (byte & utf8_header_data_mask(LENGTH)) << (6U * (LENGTH - 1U));
        }

        inline unsigned int get_utf8_character_size(unsigned char byte) {
            if (is_utf8_header_byte_for_index(byte, 4U)) {
                return 4U;
            }
            if (is_utf8_header_byte_for_index(byte, 3U)) {
                return 3U;
            }
            if (is_utf8_header_byte_for_index(byte, 2U)) {
                return 2U;
            }
            if ((byte & 0x80U) == 0U) {
                return 1U;
            }
            return 0U;
        }

        inline bool is_utf8_data_row(unsigned char byte) {
            return (byte & 0xC0U) == 0x80U;
        }

        inline char32_t transform_from_utf8_data_byte(unsigned char byte, unsigned int index) {
            return (byte & 0x3FU) << (6U * index);
        }

        struct from_utf8_transformation {
            const unsigned char* ucstring;
            std::size_t& byte_index;
            std::size_t byte_length;
        };

        bool is_valid_character_data(const from_utf8_transformation& transformation,
                                     std::size_t byte_offset,
                                     int char_position,
                                     bool current_value) {
            if (char_position == 0U) {
                return current_value;
            }
            return is_valid_character_data(transformation,
                                           byte_offset + 1UL,
                                           char_position - 1,
                                           current_value && is_utf8_data_row(transformation.ucstring[byte_offset]));
        }

        template<unsigned int CHAR_LENGTH>
        bool is_valid_character(const from_utf8_transformation& transformation) {
            if (CHAR_LENGTH == 1) {
                return transformation.byte_index < transformation.byte_length
                       && (transformation.ucstring[transformation.byte_index] & 0x80U) == 0U;
            }
            return transformation.byte_index + CHAR_LENGTH - 1 < transformation.byte_length
                   && is_valid_character_data(transformation, transformation.byte_index + 1, CHAR_LENGTH - 1, true);
        }
        // endregion

        inline int compare_to(const char32_t* lhs_data,
                              std::size_t lhs_length,
                              const char32_t* rhs_data,
                              std::size_t rhs_length) {
            auto min_length = lhs_length < rhs_length ? lhs_length : rhs_length;
            for (auto i = 0UL; i < min_length; ++i) {
                if (lhs_data[i] < rhs_data[i]) {
                    return -1;
                }
                if (lhs_data[i] > rhs_data[i]) {
                    return 1;
                }
            }
            if (lhs_length < rhs_length) {
                return -1;
            }
            if (lhs_length > rhs_length) {
                return 1;
            }
            return 0;
        }
    }

    unicode_string::unicode_string() {
        m_length = 0UL;
        m_data = nullptr;
    }

    unicode_string::unicode_string(const char32_t* cstring) {
        m_length = calculate_size_in_elements(cstring);
        m_data = new char32_t[m_length + 1UL];
        for (auto i = 0UL; i < m_length; ++i) {
            m_data[i] = cstring[i];
        }
        m_data[m_length] = 0;
    }

    unicode_string::unicode_string(const char32_t* cstring, std::size_t lengthInCharacters) {
        m_length = lengthInCharacters;
        m_data = new char32_t[lengthInCharacters + 1];
        for (auto i = 0UL; i < lengthInCharacters; ++i) {
            m_data[i] = cstring[i];
        }
        m_data[lengthInCharacters] = 0;
    }

    unicode_string::unicode_string(const char16_t* cstring, std::size_t lengthInCharacters) {
        auto size_in_words = calculate_size_in_elements(cstring);
        m_length = 0UL;
        auto temp = 0UL;
        for (; temp < size_in_words && m_length < lengthInCharacters; ++temp) {
            ++m_length;
            if ((cstring[temp] & 0xFC00U) == 0xD800U && temp + 1 < size_in_words && (cstring[temp + 1] & 0xFC00U) == 0xDC00U) {
                ++temp;
            }
        }
        size_in_words = temp;
        m_data = new char32_t[m_length + 1UL];
        m_data[m_length] = 0;

        for (auto i = 0UL; i < size_in_words; ++i) {
            if ((cstring[i] & 0xFC00U) == 0xD800U && i + 1 < size_in_words && (cstring[i + 1] & 0xFC00U) == 0xDC00U) {
                m_data[i] = (((cstring[i] & 0x03FFU) << 10U) | (cstring[i + 1] & 0x03FFU)) + 0x10000U;
                ++i;
            } else if ((cstring[i] & 0xF800U) == 0xD800U) {
                m_data[i] = 0xFFFF;
            } else {
                m_data[i] = cstring[i];
            }
        }
    }

    unicode_string::unicode_string(const char16_t* cstring) {
        auto size_in_words = calculate_size_in_elements(cstring);
        m_length = 0UL;
        for (auto i = 0UL; i < size_in_words; ++i) {
            ++m_length;
            if ((cstring[i] & 0xFC00U) == 0xD800U && i + 1 < size_in_words && (cstring[i + 1] & 0xFC00U) == 0xDC00U) {
                ++i;
            }
        }
        m_data = new char32_t[m_length + 1UL];
        m_data[m_length] = 0;

        for (auto i = 0UL; i < size_in_words; ++i) {
            if ((cstring[i] & 0xFC00U) == 0xD800U && i + 1 < size_in_words && (cstring[i + 1] & 0xFC00U) == 0xDC00U) {
                m_data[i] = (((cstring[i] & 0x03FFU) << 10U) | (cstring[i + 1] & 0x03FFU)) + 0x10000U;
                ++i;
            } else if ((cstring[i] & 0xF800U) == 0xD800U) {
                m_data[i] = 0xFFFF;
            } else {
                m_data[i] = cstring[i];
            }
        }
    }

    unicode_string::unicode_string(const char* cstring, std::size_t lengthInCharacters) {
        auto ucstring = reinterpret_cast<const unsigned char*>(cstring);
        m_length = lengthInCharacters;
        auto size_in_bytes = calculate_size_in_elements(ucstring);
        m_data = new char32_t[m_length + 1];
        std::size_t unicodeCharacterIndex = 0UL;
        for (std::size_t i = 0UL;
             i < size_in_bytes && unicodeCharacterIndex < lengthInCharacters; ++i, ++unicodeCharacterIndex) {
            auto utf8_char_size = get_utf8_character_size(ucstring[i]);
            from_utf8_transformation transformation = {
                    ucstring,
                    i,
                    size_in_bytes
            };
            if (utf8_char_size == 4U && is_valid_character<4U>(transformation)) {
                m_data[unicodeCharacterIndex] = transform_from_utf8_header_byte<4U>(ucstring[i])
                                                | transform_from_utf8_data_byte(ucstring[i + 1U], 2U)
                                                | transform_from_utf8_data_byte(ucstring[i + 2U], 1U)
                                                | transform_from_utf8_data_byte(ucstring[i + 3U], 0U);
                i += 3;
            } else if (utf8_char_size == 4U
                       && i + 2 < size_in_bytes
                       && is_utf8_data_row(ucstring[i + 1])
                       && is_utf8_data_row(ucstring[i + 2])) {
                m_data[unicodeCharacterIndex] = INVALID_UNICODE_CODEPOINT;
                i += 2;
            } else if (utf8_char_size == 4U && i + 1 < size_in_bytes && is_utf8_data_row(ucstring[i + 1])) {
                m_data[unicodeCharacterIndex] = INVALID_UNICODE_CODEPOINT;
                ++i;
            } else if (utf8_char_size == 3U && is_valid_character<3U>(transformation)) {
                m_data[unicodeCharacterIndex] = transform_from_utf8_header_byte<3U>(ucstring[i])
                                                | transform_from_utf8_data_byte(ucstring[i + 1], 1U)
                                                | transform_from_utf8_data_byte(ucstring[i + 2], 0U);
                i += 2;
            } else if (utf8_char_size == 3U && i + 1 < size_in_bytes && is_utf8_data_row(ucstring[i + 1])) {
                m_data[unicodeCharacterIndex] = INVALID_UNICODE_CODEPOINT;
                ++i;
            } else if (utf8_char_size == 2U && is_valid_character<2U>(transformation)) {
                m_data[unicodeCharacterIndex] = transform_from_utf8_header_byte<2U>(ucstring[i])
                                                | transform_from_utf8_data_byte(ucstring[i + 1], 0U);
                ++i;
            } else if (utf8_char_size == 1U) {
                m_data[unicodeCharacterIndex] = cstring[i];
            } else {
                m_data[unicodeCharacterIndex] = INVALID_UNICODE_CODEPOINT;
            }
        }
        m_data[unicodeCharacterIndex] = 0U;
    }

    unicode_string::unicode_string(const char* cstring)
            : unicode_string(cstring,
                             calculate_size_in_utf8_characters(reinterpret_cast<const unsigned char*>(cstring))) {
    }

    unicode_string::unicode_string(const music_library::core::unicode_string& rhs) {
        m_length = rhs.m_length;
        if (rhs.m_data != nullptr) {
            m_data = new char32_t[m_length + 1UL];
            for (auto i = 0UL; i < m_length; ++i) {
                m_data[i] = rhs.m_data[i];
            }
            m_data[m_length] = U'\0';
        } else {
            m_data = nullptr;
        }
    }

    unicode_string::unicode_string(music_library::core::unicode_string&& rhs) noexcept {
        m_length = rhs.m_length;
        m_data = rhs.m_data;
        rhs.m_data = nullptr;
    }

    unicode_string::~unicode_string() {
        m_length = 0UL;
        delete[] m_data;
        m_data = nullptr;
    }

    unicode_string unicode_string::from_iso_8859_1(const char* const isoString) {
        auto length = calculate_size_in_elements(isoString);
        std::vector<char32_t> buffer(length + 1);
        for (auto i = 0UL; i < length; ++i) {
            buffer[i] = static_cast<unsigned char>(isoString[i]);
        }
        buffer[length] = 0;
        return buffer.data();
    }

    std::size_t unicode_string::length() const {
        return m_length;
    }

    namespace {
        const char32_t WHITESPACE[] = U" \t\n\r";

        bool is_char_in_string(char32_t c, const char32_t* string) {
            if (*string == 0) return false;
            if (*string == c) return true;
            return is_char_in_string(c, string + 1);
        }

        bool is_whitespace(char32_t c) {
            return is_char_in_string(c, WHITESPACE);
        }
    }

    unicode_string unicode_string::left_trim() const {
        auto offset = 0UL;
        while (offset < m_length && is_whitespace(m_data[offset])) {
            ++offset;
        }
        return unicode_string(m_data + offset, m_length - offset);
    }

    unicode_string unicode_string::right_trim() const {
        auto newLength = m_length;
        while (newLength > 0UL && is_whitespace(m_data[newLength - 1])) {
            --newLength;
        }

        return unicode_string(m_data, newLength);
    }

    unicode_string unicode_string::trim() const {
        auto offset = 0UL;
        auto newLength = m_length;
        while (newLength > 0UL && is_whitespace(m_data[newLength - 1])) {
            --newLength;
        }
        while (newLength > 0UL && is_whitespace(m_data[offset])) {
            ++offset;
            --newLength;
        }
        return unicode_string(m_data + offset, newLength);
    }

    std::u32string unicode_string::to_utf32_string() const {
        if (m_data == nullptr) {
            return U"";
        }
        return m_data;
    }

    std::u16string unicode_string::to_utf16_string() const {
        auto size_in_words = 0UL;
        for (auto i = 0; i < m_length; ++i) {
            if (m_data[i] >= 0x10000U) {
                size_in_words += 2;
            } else {
                ++size_in_words;
            }
        }
        auto buffer = new char16_t[size_in_words + 1UL];
        buffer[size_in_words] = 0;
        auto current_raw_index = 0UL;
        for (auto i = 0; i < m_length; ++i) {
            if (m_data[i] < 0x10000U) {
                buffer[current_raw_index] = static_cast<char16_t>(m_data[i]);
                ++current_raw_index;
            } else {
                auto temp = m_data[i] - 0x10000U;
                auto surrogate_byte = 0xD800U | (temp >> 10U);
                auto other_byte = 0XDC00U | (temp & 0x003FFU);
                buffer[current_raw_index] = surrogate_byte;
                buffer[current_raw_index + 1] = other_byte;
                current_raw_index += 2;
            }
        }
        return std::u16string(buffer, size_in_words);
    }

    std::string unicode_string::to_utf8_string() const {
        std::size_t utf8_length = 0UL;
        for (auto i = 0UL; i < m_length; ++i) {
            if (m_data[i] < 0x80U) {
                ++utf8_length;
            } else if (m_data[i] < 0x08'00U) {
                utf8_length += 2;
            } else if (m_data[i] < 0x01'00'00U) {
                utf8_length += 3;
            } else if (m_data[i] < 0x10'FF'FFU) {
                utf8_length += 4;
            } else {
                utf8_length += 3;
            }
        }
        unsigned char* buffer = new unsigned char[utf8_length + 1];
        buffer[utf8_length] = 0;
        auto utf8_index = 0UL;
        for (auto i = 0UL; i < m_length; ++i) {
            if (m_data[i] < 0x80U) {
                buffer[utf8_index] = static_cast<unsigned char>(m_data[i]);
                ++utf8_index;
            } else if (m_data[i] < 0x08'00U) {
                buffer[utf8_index] = 0xC0U | (m_data[i] >> 6U);
                buffer[++utf8_index] = 0x80U | (m_data[i] & 0x00'00'00'3FU);
                ++utf8_index;
            } else if (m_data[i] < 0x01'00'00U) {
                buffer[utf8_index] = 0xE0U | (m_data[i] >> 12U);
                buffer[++utf8_index] = 0x80U | ((m_data[i] >> 6U) & 0x00'00'00'3FU);
                buffer[++utf8_index] = 0x80U | (m_data[i] & 0x00'00'00'3FU);
                ++utf8_index;
            } else if (m_data[i] < 0x10'FF'FFU) {
                buffer[utf8_index] = 0xF0U | (m_data[i] >> 18U);
                buffer[++utf8_index] = 0x80U | ((m_data[i] >> 12U) & 0x00'00'00'3FU);
                buffer[++utf8_index] = 0x80U | ((m_data[i] >> 6U) & 0x00'00'00'3FU);
                buffer[++utf8_index] = 0x80U | (m_data[i] & 0x00'00'00'3FU);
                ++utf8_index;
            } else {
                buffer[utf8_index] = 0xEF;
                buffer[++utf8_index] = 0xBF;
                buffer[++utf8_index] = 0xBF;
                ++utf8_index;
            }
        }
        return std::string(reinterpret_cast<char*>(buffer), utf8_length);
    }

    bool operator==(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) == 0;
    }

    bool operator!=(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) != 0;
    }

    bool operator<(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) < 0;
    }

    bool operator>(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) > 0;
    }

    bool operator<=(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) <= 0;
    }

    bool operator>=(const unicode_string& lhs, const unicode_string& rhs) {
        return compare_to(lhs.m_data, lhs.m_length, rhs.m_data, rhs.m_length) >= 0;
    }

    unicode_string operator+(const unicode_string& lhs, const unicode_string& rhs) {
        auto total_length = lhs.m_length + rhs.m_length;
        auto data = new char32_t[total_length + 1UL];
        std::size_t i = 0UL;
        for (; i < lhs.m_length; ++i) {
            data[i] = lhs.m_data[i];
        }
        for (auto j = 0UL; j < rhs.m_length; ++i, ++j) {
            data[i] = rhs.m_data[j];
        }
        data[total_length] = 0;

        unicode_string result;
        result.m_length = total_length;
        delete[] result.m_data;
        result.m_data = data;

        return result;
    }

    unicode_string& unicode_string::operator=(const music_library::core::unicode_string& rhs) {
        if (this == &rhs) {
            return *this;
        }
        auto data = new char32_t[rhs.m_length + 1UL];
        for (auto i = 0; i < rhs.m_length; ++i) {
            data[i] = rhs.m_data[i];
        }
        data[rhs.m_length] = 0;

        delete[] m_data;
        m_data = data;
        m_length = rhs.m_length;
        return *this;
    }

    unicode_string& unicode_string::operator+=(const music_library::core::unicode_string& rhs) {
        std::size_t total_length = m_length + rhs.m_length;
        auto data = new char32_t[total_length + 1UL];
        std::size_t i = 0UL;
        for (; i < m_length; ++i) {
            data[i] = m_data[i];
        }
        for (auto j = 0UL; j < rhs.m_length; ++i, ++j) {
            data[i] = rhs.m_data[j];
        }
        data[total_length] = 0;

        delete[] m_data;
        m_data = data;
        m_length = total_length;

        return *this;
    }

    namespace unicode_string_literals {
        unicode_string operator "" _us(const char* utf8String, std::size_t length) {
            return unicode_string(utf8String, length);
        }

        unicode_string operator "" _us(const char16_t* utf16String, std::size_t length) {
            return unicode_string(utf16String, length);
        }

        unicode_string operator "" _us(const char32_t* utf32String, std::size_t length) {
            return unicode_string(utf32String, length);
        }
    }
}