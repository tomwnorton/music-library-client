//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <string>
#include <vector>

#include "core/unicode-string.hpp"
#include "core/scsi-session.hpp"
#include "core/scsi.hpp"

namespace music_library::core {
    std::vector<track_info> get_table_of_contents(const scsi_session& session);

    uint16_t get_text_pack_response_size(const scsi_session& session);

    void update_vector_with_track_names(const scsi_session& session, text_info& albumInfo, std::vector<track_info>& tracks, uint16_t expectedSize);

    disc_info scsi_service::get_tracks(const unicode_string& drivePath) const {
        scsi_session session(drivePath);
        text_info albumInfo;
        std::vector<track_info> tracks = get_table_of_contents(session);
        try {
            uint16_t expectedSize = get_text_pack_response_size(session);
            update_vector_with_track_names(session, albumInfo, tracks, expectedSize);
        } catch (...) {
        }
        return {albumInfo, tracks};
    }
}