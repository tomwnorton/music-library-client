//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <cstring>

#include <functional>
#include <optional>
#include <string>
#include <vector>
#include <unordered_map>

#include "core/unicode-string.hpp"
#include "core/big-endian.hpp"
#include "core/scsi-session.hpp"
#include "core/scsi-command.hpp"
#include "core/scsi.hpp"
#include "core/scsi-metadata-request.hpp"

namespace music_library::core {
    struct track_text_data {
        unicode_string title;
        unicode_string performer;
        unicode_string song_writer;
        unicode_string composer;
        unicode_string arranger;
        std::optional<int> trackNumber;
    };

    // Not in anonymous namespace because tests trump encapsulation
    struct text_pack {
        uint8_t headers[4];
        uint8_t data[12];
        uint8_t crc[2];
    };

    namespace {
        const uint8_t READ_CD_TEXT_FORMAT = 0x05;

        struct read_cd_text_response {
            big_endian<2UL> size_in_bytes;
            uint16_t first_reserved_block;
            text_pack text_packs[];
        };
        typedef scsi_command<0x43, read_toc_pma_atip_request, big_endian<2UL>> cd_text_length_command;
        typedef scsi_command<0x43, read_toc_pma_atip_request, read_cd_text_response> cd_text_command;

        int copy_text_pack_text(char* dest, const uint8_t* src, int startIndex) {
            auto index = startIndex;

            do {
                dest[index - startIndex] = src[index];
                ++index;
            } while (index < 12 && src[index - 1] != '\0');

            if (index == 12) {
                dest[index - startIndex] = 0;
            }

            return index;
        }

        track_text_data* get_data_at_index(std::vector<track_text_data>& vector, int index) {
            while (vector.size() <= index) {
                track_text_data data;
                vector.push_back(data);
            }
            return &vector[index];
        }

        void copy_data_to_text_info(text_info& text, const track_text_data& data) {
            text.title = data.title;
            text.performer = data.performer;
            text.song_writer = data.song_writer;
            text.composer = data.composer;
            text.arranger = data.arranger;
        }
    }

    void process_text_packs(const text_pack* textPacks, std::function<void(const track_text_data&)> handler) {
        char buffer[13];
        buffer[12] = 0;

        // @formatter:off
        std::unordered_map<uint8_t, unicode_string track_text_data::*> string_map({
            { 0x80, &track_text_data::title },
            { 0x81, &track_text_data::performer },
            { 0x82, &track_text_data::song_writer },
            { 0x83, &track_text_data::composer },
            { 0x84, &track_text_data::arranger }
        });
        // @formatter:on

        std::vector<track_text_data> results;

        int maxTrackNumber;
        for (auto textPack = textPacks;; ++textPack) {
            if (textPack->headers[0] == 0x8F) {
                maxTrackNumber = textPack->data[2];
                break;
            }
        }

        for (auto textPack = textPacks; textPack->headers[0] != 0x8F; ++textPack) {
            if (string_map.find(textPack->headers[0]) != string_map.end()) {
                int offset = 0;
                int lastStartIndex = 0;
                do {
                    auto data = get_data_at_index(results, textPack->headers[1] + offset);
                    if (textPack->headers[1] + offset > 0) {
                        data->trackNumber = textPack->headers[1] + offset;
                    }
                    lastStartIndex = copy_text_pack_text(buffer, textPack->data, lastStartIndex);
                    data->*string_map[textPack->headers[0]] += unicode_string::from_iso_8859_1(buffer);
                    ++offset;
                } while (lastStartIndex != 12 && textPack->headers[1] + offset <= maxTrackNumber);
            }
        }
        for (auto data : results) {
            handler(data);
        }
    }

    uint16_t get_text_pack_response_size(const scsi_session& session) {
        cd_text_length_command command;
        command.request().format = READ_CD_TEXT_FORMAT;
        command.request().allocation_length = 2;
        uint16_t expectedSize;
        command.execute(session, [&](big_endian<2UL> size) { expectedSize = size; });
        return expectedSize + 4;
    }

    void update_vector_with_track_names(const scsi_session& session, text_info& albumInfo, std::vector<track_info>& tracks, uint16_t expectedSize) {
        cd_text_command cdTextCommand;
        cdTextCommand.request().format = READ_CD_TEXT_FORMAT;
        cdTextCommand.request().allocation_length = expectedSize;
        cdTextCommand.execute(session,
                              [&](const read_cd_text_response& responseModel) {
                                  process_text_packs(responseModel.text_packs, [&](const track_text_data& data) {
                                      if (!data.trackNumber) {
                                          copy_data_to_text_info(albumInfo, data);
                                      }
                                      if (data.trackNumber && *data.trackNumber <= tracks.size()) {
                                          auto index = *data.trackNumber - 1;
                                          copy_data_to_text_info(tracks[index].text, data);
                                      }
                                  });
                              },
                              expectedSize);
    }
}