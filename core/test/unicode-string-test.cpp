//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdint>
#include <ostream>
#include <string>
#include <catch2/catch.hpp>
#include "core/unicode-string.hpp"

namespace Catch {
	template<>
	struct StringMaker<music_library::core::unicode_string> {
		static std::string convert(const music_library::core::unicode_string& value) {
#ifdef _WIN32
			auto utf32_string = value.to_utf32_string();
			auto length = 0ULL;
			for (auto i = 0ULL; i < utf32_string.length(); ++i) {
				if (utf32_string[i] <= 0x7FULL) {
					++length;
				} else {
					std::uint32_t temp_char = utf32_string[i];
					while (temp_char != 0U) {
						++i;
						temp_char /= 0x10U;
					}
					length += i + 2ULL;
				}
			}
			auto temp = new char[length + 1ULL];
			temp[length] = 0;
			auto current_dest_index = 0ULL;
			for (auto i = 0ULL; i < utf32_string.length(); ++i) {
				std::uint32_t temp_char = utf32_string[i];
				if (temp_char <= 0x7FULL) {
					temp[current_dest_index] = static_cast<char>(utf32_string[i]);
					++current_dest_index;
				} else {
					const char HEX_DIGITS[17ULL] = "0123456789ABCDEF";
					char sequence[6];
					int char_index = -1;
					while (temp_char != 0U) {
						sequence[++i] = HEX_DIGITS[temp_char % 0x10U];
						temp_char /= 0x10U;
					}
					++i;
					temp[current_dest_index] = '\\';
					temp[++current_dest_index] = 'U';
					while (i > 0) {
						temp[++current_dest_index] = sequence[--i];
					}
				}
			}
			return std::string(temp, length);
#else
			return std::string("\"") + value.to_utf8_string() + "\"";
#endif
		}
	};
}

/*std::ostream& operator<<(std::ostream& out, const music_library::core::unicode_string& str) {
    out << str.to_utf8_string();
    return out;
}*/

using music_library::core::unicode_string;

TEST_CASE("a unicode_string") {
    unicode_string the_string;

    SECTION("is blank by default") {
        CHECK(the_string.length() == 0UL);
        CHECK(the_string.to_utf32_string() == U"");
        CHECK_FALSE(the_string.to_utf32_string() == U"c");
    }

    SECTION("can be initialized with a UTF-32 string") {
        CHECK(static_cast<unicode_string>(U"hello").length() == 5UL);
        CHECK(static_cast<unicode_string>(U"a").to_utf32_string() == U"a");
        CHECK_FALSE(static_cast<unicode_string>(U"b").to_utf32_string() == U"a");
        CHECK(static_cast<unicode_string>(U"ab").to_utf32_string() == U"ab");
        CHECK_FALSE(static_cast<unicode_string>(U"ac").to_utf32_string() == U"ab");
    }

    SECTION("can be compared using equals (==)") {
        CHECK(static_cast<unicode_string>(U"a") == static_cast<unicode_string>(U"a"));
        CHECK_FALSE(static_cast<unicode_string>(U"a") == static_cast<unicode_string>(U"b"));
        CHECK(static_cast<unicode_string>(U"ab") == static_cast<unicode_string>(U"ab"));
        CHECK_FALSE(static_cast<unicode_string>(U"ab") == static_cast<unicode_string>(U"af"));
        CHECK_FALSE(static_cast<unicode_string>(U"ab") == static_cast<unicode_string>(U"abf"));
    }

    SECTION("can be compared using not-equals (!=)") {
        CHECK(static_cast<unicode_string>(U"a") != static_cast<unicode_string>(U"b"));
        CHECK_FALSE(static_cast<unicode_string>(U"a") != static_cast<unicode_string>(U"a"));
        CHECK(static_cast<unicode_string>(U"a") != static_cast<unicode_string>(U"ab"));
        CHECK(static_cast<unicode_string>(U"af") != static_cast<unicode_string>(U"ab"));
    }

    SECTION("can be compared using less-than (<)") {
        CHECK(static_cast<unicode_string>(U"a") < static_cast<unicode_string>(U"b"));
        CHECK_FALSE(static_cast<unicode_string>(U"a") < static_cast<unicode_string>(U"a"));
        CHECK_FALSE(static_cast<unicode_string>(U"b") < static_cast<unicode_string>(U"a"));
        CHECK(static_cast<unicode_string>(U"a") < static_cast<unicode_string>(U"aa"));
        CHECK_FALSE(static_cast<unicode_string>(U"b") < static_cast<unicode_string>(U"aa"));
        CHECK(static_cast<unicode_string>(U"ab") < static_cast<unicode_string>(U"ac"));
    }

    SECTION("can be compared using greater-than (>)") {
        CHECK_FALSE(static_cast<unicode_string>(U"") > static_cast<unicode_string>(U""));
        CHECK(static_cast<unicode_string>(U"a") > static_cast<unicode_string>(U""));
        CHECK_FALSE(static_cast<unicode_string>(U"") > static_cast<unicode_string>(U"a"));
        CHECK(static_cast<unicode_string>(U"b") > static_cast<unicode_string>(U"a"));
        CHECK_FALSE(static_cast<unicode_string>(U"a") > static_cast<unicode_string>(U"b"));
        CHECK(static_cast<unicode_string>(U"b") > static_cast<unicode_string>(U"aa"));
        CHECK(static_cast<unicode_string>(U"bza") > static_cast<unicode_string>(U"bz"));
    }

    SECTION("can be concatenated with another unicode_string") {
        CHECK(static_cast<unicode_string>(U"hello ") + static_cast<unicode_string>(U"world") == U"hello world");
    }

    SECTION("can be reassigned to another variable") {
        unicode_string first(U"hello");
        first = U"world";
        CHECK(first == U"world");
    }

    SECTION("can be reassigned and concatenated at the same time") {
        unicode_string first(U"hello ");
        first += U"world";
        CHECK(first == U"hello world");
    }

    SECTION("can be converted from UTF-8") {
        SECTION("can be converted from a 1-byte UTF-8 character") {
            CHECK(static_cast<unicode_string>("A") == static_cast<unicode_string>(U"A"));
            CHECK(static_cast<unicode_string>("\x7F") == static_cast<unicode_string>(U"\u007F"));
        }

        SECTION("can be converted from a series of 1-byte UTF-8 characters (U+00000 - U+0007F)") {
            CHECK(static_cast<unicode_string>("hello, world!") == static_cast<unicode_string>(U"hello, world!"));
        }

        SECTION("can be converted from a 2-byte UTF-8 character (U+00080 - U+007FF)") {
			const char temp[3] = u8"\u0080";
            CHECK(static_cast<unicode_string>(u8"\u0080") == static_cast<unicode_string>(U"\u0080"));
            CHECK(static_cast<unicode_string>(u8"\u01FC") == static_cast<unicode_string>(U"\u01FC"));
            CHECK(static_cast<unicode_string>(u8"\u07FF") == static_cast<unicode_string>(U"\u07FF"));
        }

        SECTION("can be converted from a series of 1-byte and 2-byte UTF-8 characters") {
            CHECK(static_cast<unicode_string>(u8"\u01FC\u05E5") == static_cast<unicode_string>(U"\u01FC\u05E5"));
            CHECK(static_cast<unicode_string>(u8"\u01FCA\u05E5") == static_cast<unicode_string>(U"\u01FCA\u05E5"));
        }

        SECTION("correctly handles an invalid UTF-8 2-byte character sequence") {
            CHECK(static_cast<unicode_string>("\xC0") == static_cast<unicode_string>(U"\uFFFF"));
            CHECK(static_cast<unicode_string>("\xC0\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xC0\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xC0\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xC0\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));
        }

        SECTION("can be converted from a 3-byte UTF-8 character (U+00800 - U+0FFFF)") {
            CHECK(static_cast<unicode_string>(u8"\u0800") == static_cast<unicode_string>(U"\u0800"));
            CHECK(static_cast<unicode_string>(u8"\u08FF") == static_cast<unicode_string>(U"\u08FF"));
            CHECK(static_cast<unicode_string>(u8"\uFFFF") == static_cast<unicode_string>(U"\uFFFF"));
        }

        SECTION("correctly handles an invalid UTF-8 3-byte character sequence") {
            CHECK(static_cast<unicode_string>("\xE0") == static_cast<unicode_string>(U"\uFFFF"));
            CHECK(static_cast<unicode_string>("\xE0\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xE0\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xE0\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xE0\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));
            CHECK(static_cast<unicode_string>("\xE0\x01\x41") == static_cast<unicode_string>(U"\uFFFF\u0001A"));
            CHECK(static_cast<unicode_string>("\xE0\x3F\x41") == static_cast<unicode_string>(U"\uFFFF\u003FA"));
            CHECK(static_cast<unicode_string>("\xE0\x7F\x41") == static_cast<unicode_string>(U"\uFFFF\u007FA"));
            CHECK(static_cast<unicode_string>("\xE0\xFF\x41") == static_cast<unicode_string>(U"\uFFFF\uFFFFA"));
            CHECK(static_cast<unicode_string>("\xE0\x80\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xE0\x80\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xE0\x80\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xE0\x80\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));
        }

        SECTION("can be converted from a 4-byte UTF-8 character (U+10000 - U+10FFFF)") {
            CHECK(static_cast<unicode_string>(u8"\U00010000") == static_cast<unicode_string>(U"\U00010000"));
            CHECK(static_cast<unicode_string>(u8"\U0001001B") == static_cast<unicode_string>(U"\U0001001B"));
            CHECK(static_cast<unicode_string>(u8"\U000101F2") == static_cast<unicode_string>(U"\U000101F2"));
            CHECK(static_cast<unicode_string>(u8"\U000E0125") == static_cast<unicode_string>(U"\U000E0125"));
        }

        SECTION("correctly handles an invalid UTF-8 4-byte character sequence") {
            CHECK(static_cast<unicode_string>("\xF0") == static_cast<unicode_string>(U"\uFFFF"));
            CHECK(static_cast<unicode_string>("\xF0\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xF0\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xF0\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xF0\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));
            CHECK(static_cast<unicode_string>("\xF0\x01\x41") == static_cast<unicode_string>(U"\uFFFF\u0001A"));
            CHECK(static_cast<unicode_string>("\xF0\x3F\x41") == static_cast<unicode_string>(U"\uFFFF\u003FA"));
            CHECK(static_cast<unicode_string>("\xF0\x7F\x41") == static_cast<unicode_string>(U"\uFFFF\u007FA"));
            CHECK(static_cast<unicode_string>("\xF0\xFF\x41") == static_cast<unicode_string>(U"\uFFFF\uFFFFA"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xF0\x80\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));

            CHECK(static_cast<unicode_string>("\xF0\x80\x80\x01") == static_cast<unicode_string>(U"\uFFFF\u0001"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x80\x3F") == static_cast<unicode_string>(U"\uFFFF\u003F"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x80\x7F") == static_cast<unicode_string>(U"\uFFFF\u007F"));
            CHECK(static_cast<unicode_string>("\xF0\x80\x80\xFF") == static_cast<unicode_string>(U"\uFFFF\uFFFF"));
        }

        SECTION("correctly handles raw strings") {
            const char* fullString = "Hello, world!";
            CHECK(unicode_string(fullString, 5UL) == static_cast<unicode_string>("Hello"));
        }
    }

    SECTION("can be converted to UTF-8") {
        SECTION("Codepoints in the range of U+00000 - U+0007F can be converted into a 1-byte UTF-8 sequence") {
            CHECK(static_cast<unicode_string>(U"A").to_utf8_string() == "A");
            CHECK(static_cast<unicode_string>(U"\u007F").to_utf8_string() == u8"\u007F");
            CHECK(static_cast<unicode_string>(U"Hello, world!").to_utf8_string() == "Hello, world!");
        }

        SECTION("Codepoints in the range of U+00080 - U+007FF can be converted into a 2-byte UTF-8 sequence") {
            CHECK(static_cast<unicode_string>(U"\u0080").to_utf8_string() == u8"\u0080");
            CHECK(static_cast<unicode_string>(U"\u01FC").to_utf8_string() == u8"\u01FC");
            CHECK(static_cast<unicode_string>(U"\u07FF").to_utf8_string() == u8"\u07FF");
        }

        SECTION("Codepoints in the range of U+00800 - U+0FFFF can be converted into a 3-byte UTF-8 sequence") {
            CHECK(static_cast<unicode_string>(U"\u0800").to_utf8_string() == u8"\u0800");
            CHECK(static_cast<unicode_string>(U"\u08FF").to_utf8_string() == u8"\u08FF");
            CHECK(static_cast<unicode_string>(U"\uFFFF").to_utf8_string() == u8"\uFFFF");
        }

        SECTION("Codepoints in the range of U+10000 - U+10FFFF can be converted into a 4-byte UTF-8 sequence") {
            CHECK(static_cast<unicode_string>(U"\U00010000").to_utf8_string() == u8"\U00010000");
            CHECK(static_cast<unicode_string>(U"\U0001001B").to_utf8_string() == u8"\U0001001B");
            CHECK(static_cast<unicode_string>(U"\U000101F2").to_utf8_string() == u8"\U000101F2");
            CHECK(static_cast<unicode_string>(U"\U000E0125").to_utf8_string() == u8"\U000E0125");
        }

        SECTION("Invalid codepoints are encoded as the Invalid character (U+FFFF)") {
            char32_t invalid_unicode_string[2] = { 0x11'FF'FF, 0 };
            CHECK(static_cast<unicode_string>(invalid_unicode_string).to_utf8_string() == u8"\uFFFF");
        }

        SECTION("A string with codepoints all over the map can be properly converted to UTF-8") {
            CHECK(static_cast<unicode_string>(U"\u01FChello\U0001001B \u08FFworld").to_utf8_string() == u8"\u01FChello\U0001001B \u08FFworld");
        }
    }

    SECTION("can be converted from a UTF-16 LE word array") {
        SECTION("can be converted from a 2-byte UTF-16 character (U+00000 - U+0D7FF, U+0E000 - U+0FFFF)") {
            CHECK(static_cast<unicode_string>(u"hello, world!") == static_cast<unicode_string>(U"hello, world!"));
            CHECK(static_cast<unicode_string>(u"\uD7FF") == static_cast<unicode_string>(U"\uD7FF"));
            CHECK(static_cast<unicode_string>(u"\uE000") == static_cast<unicode_string>(U"\uE000"));
            CHECK(static_cast<unicode_string>(u"\uFFFF") == static_cast<unicode_string>(U"\uFFFF"));
        }

        SECTION("converts an invalid character into the Invalid Character codepoint (U+0FFFF)") {
            char16_t invalid_string[2] = { 0xD8'00, 0 };
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xD8'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xD9'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDA'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDB'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDC'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDD'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDE'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));

            invalid_string[0] = 0xDF'FF;
            CHECK(static_cast<unicode_string>(invalid_string) == static_cast<unicode_string>(U"\uFFFF"));
        }

        SECTION("can be converted from a 4-byte UTF-16 LE word array") {
            CHECK(static_cast<unicode_string>(u"\U00010000") == static_cast<unicode_string>(U"\U00010000"));
            CHECK(static_cast<unicode_string>(u"\U0001001B") == static_cast<unicode_string>(U"\U0001001B"));
            CHECK(static_cast<unicode_string>(u"\U000101F2") == static_cast<unicode_string>(U"\U000101F2"));
            CHECK(static_cast<unicode_string>(u"\U000E0125") == static_cast<unicode_string>(U"\U000E0125"));
            CHECK(static_cast<unicode_string>(u"\U0010FFFF") == static_cast<unicode_string>(U"\U0010FFFF"));
        }

        SECTION("correctly handles raw strings") {
            const char16_t* fullString = u"Hello, world!";
            CHECK(unicode_string(fullString, 5UL) == static_cast<unicode_string>("Hello"));
        }
    }

    SECTION("can be converted to a UTF-16 LE word array") {
        SECTION("can be converted to a series of 2-byte values (U+00000 - U+0D7FF, U+0E000 - U+0FFFF)") {
            CHECK(static_cast<unicode_string>(U"Hello, world!").to_utf16_string() == u"Hello, world!");
            CHECK(static_cast<unicode_string>(U"\uD7FF").to_utf16_string() == u"\uD7FF");
            CHECK(static_cast<unicode_string>(U"\uE000").to_utf16_string() == u"\uE000");
            CHECK(static_cast<unicode_string>(U"\uFFFF").to_utf16_string() == u"\uFFFF");
        }

        SECTION("can be converted to a series of 4-byte values (U+10000 - U+10FFFF)") {
            CHECK(static_cast<unicode_string>(U"\U00010000").to_utf16_string() == u"\U00010000");
            CHECK(static_cast<unicode_string>(U"\U0001001B").to_utf16_string() == u"\U0001001B");
            CHECK(static_cast<unicode_string>(U"\U000101F2").to_utf16_string() == u"\U000101F2");
            CHECK(static_cast<unicode_string>(U"\U000E0125").to_utf16_string() == u"\U000E0125");
            CHECK(static_cast<unicode_string>(U"\U0010FFFF").to_utf16_string() == u"\U0010FFFF");
        }
    }

    SECTION("can be trimmed from the left") {
        unicode_string original = " \t \n  \r hello   \t\n \r  ";
        CHECK(static_cast<unicode_string>("").left_trim() == static_cast<unicode_string>(""));
        CHECK(static_cast<unicode_string>("    ").left_trim() == static_cast<unicode_string>(""));
        CHECK(original.left_trim() == static_cast<unicode_string>("hello   \t\n \r  "));
    }

    SECTION("can be trimmed from the right") {
        unicode_string original = " \t \n  \r hello   \t\n \r  ";
        CHECK(static_cast<unicode_string>("").right_trim() == static_cast<unicode_string>(""));
        CHECK(static_cast<unicode_string>("    ").right_trim() == static_cast<unicode_string>(""));
        CHECK(original.right_trim() == static_cast<unicode_string>(" \t \n  \r hello"));
    }

    SECTION("can be trimmed from both sides") {
        unicode_string original = " \t \n  \r hello   \t\n \r  ";
        CHECK(static_cast<unicode_string>("").trim() == static_cast<unicode_string>(""));
        CHECK(static_cast<unicode_string>("    ").trim() == static_cast<unicode_string>(""));
        CHECK(original.trim() == static_cast<unicode_string>("hello"));
    }

    SECTION("can be converted from a raw string of ISO-8859-1 characters") {
        char iso_8859_1[256];
        char32_t unicode[256];

        for (auto i = 1; i < 256; ++i) {
            iso_8859_1[i - 1] = i;
            unicode[i - 1] = i;
        }
        iso_8859_1[255] = 0;
        unicode[255] = 0;

        unicode_string expected = unicode;
        auto actual = unicode_string::from_iso_8859_1(iso_8859_1);

        CHECK(expected == actual);
    }
}