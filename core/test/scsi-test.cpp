//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include <cstdint>
#include <ostream>
#include <string>
#include <catch2/catch.hpp>
#include "core/unicode-string.hpp"
#include "core/scsi.hpp"

namespace Catch {
    template<>
    struct StringMaker<music_library::core::unicode_string> {
        static std::string convert(const music_library::core::unicode_string& value) {
#ifdef _WIN32
            auto utf32_string = value.to_utf32_string();
            auto length = 0ULL;
            for (auto i = 0ULL; i < utf32_string.length(); ++i) {
                if (utf32_string[i] <= 0x7FULL) {
                    ++length;
                } else {
                    std::uint32_t temp_char = utf32_string[i];
                    while (temp_char != 0U) {
                        ++i;
                        temp_char /= 0x10U;
                    }
                    length += i + 2ULL;
                }
            }
            auto temp = new char[length + 1ULL];
            temp[length] = 0;
            auto current_dest_index = 0ULL;
            for (auto i = 0ULL; i < utf32_string.length(); ++i) {
                std::uint32_t temp_char = utf32_string[i];
                if (temp_char <= 0x7FULL) {
                    temp[current_dest_index] = static_cast<char>(utf32_string[i]);
                    ++current_dest_index;
                } else {
                    const char HEX_DIGITS[17ULL] = "0123456789ABCDEF";
                    char sequence[6];
                    int char_index = -1;
                    while (temp_char != 0U) {
                        sequence[++i] = HEX_DIGITS[temp_char % 0x10U];
                        temp_char /= 0x10U;
                    }
                    ++i;
                    temp[current_dest_index] = '\\';
                    temp[++current_dest_index] = 'U';
                    while (i > 0) {
                        temp[++current_dest_index] = sequence[--i];
                    }
                }
            }
            return std::string(temp, length);
#else
            return value.to_utf8_string();
#endif
        }
    };
}

using music_library::core::track_length;

void check_track_length(std::uint32_t sectors, const char* expected) {
    unsigned int expectedMinutes;
    unsigned int expectedSeconds;
    unsigned int expectedFrames;

    if (std::sscanf(expected, "%u:%2u:%2u", &expectedMinutes, &expectedSeconds, &expectedFrames) != 3) { // NOLINT(cert-err34-c)
        FAIL("Could not determine expected fields!");
    }
    track_length length(sectors);
    CHECK(expectedMinutes == length.get_minutes());
    CHECK(expectedSeconds == length.get_seconds());
    CHECK(expectedFrames == length.get_frames());
}

TEST_CASE("A track_length") {
    SECTION("has the correct time fields when there is less than a second worth of sectors") {
        check_track_length(1, "00:00:01");
        check_track_length(10, "00:00:10");
    }

    SECTION("has the correct time fields when there is less than a minute worth of sectors") {
        check_track_length(80, "00:01:05");
        check_track_length(195, "00:02:45");
    }

    SECTION("has the correct time fields when there is a minute or more worth of sectors") {
        check_track_length(4500, "01:00:00");
        check_track_length(6800, "01:30:50");
    }
}