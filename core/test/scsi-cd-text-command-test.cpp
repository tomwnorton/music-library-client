//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <iostream>
#include <functional>
#include <optional>
#include <vector>
#include "core/unicode-string.hpp"

using music_library::core::unicode_string;
std::ostream& operator << (std::ostream& out, const std::optional<int>& anOptional) {
    if (anOptional) {
        out << *anOptional;
    } else {
        out << "std::nullopt";
    }
    return out;
}

#include <catch2/catch.hpp>

namespace music_library::core {
    struct text_pack {
        uint8_t headers[4];
        uint8_t data[12];
        uint8_t crc[2];
    };

    struct track_text_data {
        unicode_string title;
        unicode_string performer;
        unicode_string song_writer;
        unicode_string composer;
        unicode_string arranger;
        std::optional<int> trackNumber;
    };
    void process_text_packs(const text_pack* textPacks, std::function<void (const track_text_data&)>);
}
using music_library::core::text_pack;
using music_library::core::track_text_data;
using music_library::core::process_text_packs;

// region Real Text Pack Sample

// @formatter:off
const text_pack TEXT_PACKS[] = {
        { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    't',    'o',    ' ',    'T',    'r',    'a',    'i',    'n' }, { 0x20, 0x4F } },
        { { 0x80, 0x00, 0x01, 0x0C }, {    ' ',    'Y',    'o',    'u',    'r',    ' ',    'D',    'r',    'a',    'g',    'o',    'n' }, { 0x85, 0x2F } },
        { { 0x80, 0x00, 0x02, 0x0F }, { '\x00',    'T',    'h',    'i',    's',    ' ',    'I',    's',    ' ',    'B',    'e',    'r' }, { 0x92, 0xB4 } },
        { { 0x80, 0x01, 0x03, 0x0B }, {    'k', '\x00',    'D',    'r',    'a',    'g',    'o',    'n',    ' ',    'B',    'a',    't' }, { 0x61, 0xBB } },
        { { 0x80, 0x02, 0x04, 0x0A }, {    't',    'l',    'e', '\x00',    'T',    'h',    'e',    ' ',    'D',    'o',    'w',    'n' }, { 0x24, 0x89 } },
        { { 0x80, 0x03, 0x05, 0x08 }, {    'e',    'd',    ' ',    'D',    'r',    'a',    'g',    'o',    'n', '\x00',    'D',    'r' }, { 0xDA, 0x5F } },
        { { 0x80, 0x04, 0x06, 0x02 }, {    'a',    'g',    'o',    'n',    ' ',    'T',    'r',    'a',    'i',    'n',    'i',    'n' }, { 0x37, 0x93 } },
        { { 0x80, 0x04, 0x07, 0x0E }, {    'g', '\x00',    'W',    'o',    'u',    'n',    'd',    'e',    'd', '\x00',    'T',    'h' }, { 0x1F, 0x43 } },
        { { 0x80, 0x06, 0x08, 0x02 }, {    'e',    ' ',    'D',    'r',    'a',    'g',    'o',    'n',    ' ',    'B',    'o',    'o' }, { 0x0B, 0x64 } },
        { { 0x80, 0x06, 0x09, 0x0E }, {    'k', '\x00',    'F',    'o',    'c',    'u',    's',    ',',    ' ',    'H',    'i',    'c' }, { 0x27, 0x00 } },
        { { 0x80, 0x07, 0x0A, 0x0A }, {    'c',    'u',    'p',    '!', '\x00',    'F',    'o',    'r',    'b',    'i',    'd',    'd' }, { 0xE1, 0xCA } },
        { { 0x80, 0x08, 0x0B, 0x07 }, {    'e',    'n',    ' ',    'F',    'r',    'i',    'e',    'n',    'd',    's',    'h',    'i' }, { 0x40, 0xA5 } },
        { { 0x80, 0x08, 0x0C, 0x0F }, {    'p', '\x00',    'N',    'e',    'w',    ' ',    'T',    'a',    'i',    'l', '\x00',    'S' }, { 0xB5, 0x83 } },
        { { 0x80, 0x0A, 0x0D, 0x01 }, {    'e',    'e',    ' ',    'Y',    'o',    'u',    ' ',    'T',    'o',    'm',    'o',    'r' }, { 0x89, 0x8F } },
        { { 0x80, 0x0A, 0x0E, 0x0D }, {    'r',    'o',    'w', '\x00',    'T',    'e',    's',    't',    ' ',    'D',    'r',    'i' }, { 0x66, 0xE8 } },
        { { 0x80, 0x0B, 0x0F, 0x08 }, {    'v',    'e', '\x00',    'N',    'o',    't',    ' ',    'S',    'o',    ' ',    'F',    'i' }, { 0xE9, 0x72 } },
        { { 0x80, 0x0C, 0x10, 0x09 }, {    'r',    'e',    'p',    'r',    'o',    'o',    'f', '\x00',    'T',    'h',    'i',    's' }, { 0x59, 0x47 } },
        { { 0x80, 0x0D, 0x11, 0x04 }, {    ' ',    'T',    'i',    'm',    'e',    ' ',    'f',    'o',    'r',    ' ',    'S',    'u' }, { 0xE2, 0x97 } },
        { { 0x80, 0x0D, 0x12, 0x0F }, {    'r',    'e', '\x00',    'A',    's',    't',    'r',    'i',    'd',    ' ',    'G',    'o' }, { 0x1B, 0xB6 } },
        { { 0x80, 0x0E, 0x13, 0x09 }, {    'e',    's',    ' ',    'f',    'o',    'r',    ' ',    'a',    ' ',    'S',    'p',    'i' }, { 0x15, 0x3F } },
        { { 0x80, 0x0E, 0x14, 0x0F }, {    'n', '\x00',    'R',    'o',    'm',    'a',    'n',    't',    'i',    'c',    ' ',    'F' }, { 0xDA, 0x47 } },
        { { 0x80, 0x0F, 0x15, 0x0A }, {    'l',    'i',    'g',    'h',    't', '\x00',    'D',    'r',    'a',    'g',    'o',    'n' }, { 0xF3, 0x04 } },
        { { 0x80, 0x10, 0x16, 0x06 }, {   '\'',    's',    ' ',    'D',    'e',    'n', '\x00',    'T',    'h',    'e',    ' ',    'C' }, { 0xAE, 0x2C } },
        { { 0x80, 0x11, 0x17, 0x05 }, {    'o',    'v',    'e', '\x00',    'T',    'h',    'e',    ' ',    'K',    'i',    'l',    'l' }, { 0xE4, 0xC2 } },
        { { 0x80, 0x12, 0x18, 0x08 }, {    ' ',    'R',    'i',    'n',    'g', '\x00',    'R',    'e',    'a',    'd',    'y',    ' ' }, { 0x9D, 0x3A } },
        { { 0x80, 0x13, 0x19, 0x06 }, {    't',    'h',    'e',    ' ',    'S',    'h',    'i',    'p',    's', '\x00',    'B',    'a' }, { 0x8F, 0x33 } },
        { { 0x80, 0x14, 0x1A, 0x02 }, {    't',    't',    'l',    'i',    'n',    'g',    ' ',    't',    'h',    'e',    ' ',    'G' }, { 0x0C, 0x3F } },
        { { 0x80, 0x14, 0x1B, 0x0E }, {    'r',    'e',    'e',    'n',    ' ',    'D',    'e',    'a',    't',    'h', '\x00',    'C' }, { 0xEB, 0x49 } },
        { { 0x80, 0x15, 0x1C, 0x01 }, {    'o',    'u',    'n',    't',    'e',    'r',    ' ',    'A',    't',    't',    'a',    'c' }, { 0x89, 0x7F } },
        { { 0x80, 0x15, 0x1D, 0x0D }, {    'k', '\x00',    'W',    'h',    'e',    'r',    'e',   '\'',    's',    ' ',    'H',    'i' }, { 0x9E, 0x85 } },
        { { 0x80, 0x16, 0x1E, 0x0A }, {    'c',    'c',    'u',    'p',    '?', '\x00',    'C',    'o',    'm',    'i',    'n',    'g' }, { 0x48, 0x09 } },
        { { 0x80, 0x17, 0x1F, 0x06 }, {    ' ',    'B',    'a',    'c',    'k',    ' ',    'A',    'r',    'o',    'u',    'n',    'd' }, { 0x91, 0x00 } },
        { { 0x80, 0x17, 0x20, 0x0F }, { '\x00',    'S',    't',    'i',    'c',    'k',    's',    ' ',    '&',    ' ',    'S',    't' }, { 0x71, 0x8F } },
        { { 0x80, 0x18, 0x21, 0x0B }, {    'o',    'n',    'e', '\x00',    'T',    'h',    'e',    ' ',    'V',    'i',    'k',    'i' }, { 0xA5, 0xB5 } },
        { { 0x80, 0x19, 0x22, 0x08 }, {    'n',    'g',    's',    ' ',    'H',    'a',    'v',    'e',    ' ',    'T',    'h',    'e' }, { 0xE7, 0x7B } },
        { { 0x80, 0x19, 0x23, 0x0F }, {    'i',    'r',    ' ',    'T',    'e',    'a', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x36, 0x27 } },
        { { 0x81, 0x00, 0x24, 0x00 }, {    ' ', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x6C, 0x51 } },
        { { 0x81, 0x01, 0x25, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xF3, 0x6C } },
        { { 0x81, 0x02, 0x26, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x9D, 0xEA } },
        { { 0x81, 0x03, 0x27, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x48, 0x77 } },
        { { 0x81, 0x04, 0x28, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xAB, 0x8D } },
        { { 0x81, 0x05, 0x29, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x7E, 0x10 } },
        { { 0x81, 0x06, 0x2A, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x10, 0x96 } },
        { { 0x81, 0x07, 0x2B, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xC5, 0x0B } },
        { { 0x81, 0x08, 0x2C, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xEA, 0xDF } },
        { { 0x81, 0x09, 0x2D, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x3F, 0x42 } },
        { { 0x81, 0x0A, 0x2E, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x51, 0xC4 } },
        { { 0x81, 0x0B, 0x2F, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x84, 0x59 } },
        { { 0x81, 0x0C, 0x30, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xA1, 0x54 } },
        { { 0x81, 0x0D, 0x31, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x74, 0xC9 } },
        { { 0x81, 0x0E, 0x32, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x1A, 0x4F } },
        { { 0x81, 0x0F, 0x33, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xCF, 0xD2 } },
        { { 0x81, 0x10, 0x34, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xAE, 0x8C } },
        { { 0x81, 0x11, 0x35, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x7B, 0x11 } },
        { { 0x81, 0x12, 0x36, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x15, 0x97 } },
        { { 0x81, 0x13, 0x37, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xC0, 0x0A } },
        { { 0x81, 0x14, 0x38, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x23, 0xF0 } },
        { { 0x81, 0x15, 0x39, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xF6, 0x6D } },
        { { 0x81, 0x16, 0x3A, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x98, 0xEB } },
        { { 0x81, 0x17, 0x3B, 0x0A }, {    'l', '\x00',    'J',   0xF3,    'n',    's',    'i', '\x00',    'J',    'o',    'h',    'n' }, { 0x49, 0x87 } },
        { { 0x81, 0x19, 0x3C, 0x04 }, {    ' ',    'P',    'o',    'w',    'e',    'l',    'l', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0xD8, 0xD0 } },
        { { 0x83, 0x00, 0x3D, 0x00 }, {    ' ', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x7A, 0xC5 } },
        { { 0x83, 0x01, 0x3E, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x13, 0x3A } },
        { { 0x83, 0x02, 0x3F, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x8B, 0x7E } },
        { { 0x83, 0x03, 0x40, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xCE, 0x68 } },
        { { 0x83, 0x04, 0x41, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xCD, 0x9E } },
        { { 0x83, 0x05, 0x42, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xEE, 0xC1 } },
        { { 0x83, 0x06, 0x43, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x76, 0x85 } },
        { { 0x83, 0x07, 0x44, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xA8, 0x7F } },
        { { 0x83, 0x08, 0x45, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x8C, 0xCC } },
        { { 0x83, 0x09, 0x46, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xAF, 0x93 } },
        { { 0x83, 0x0A, 0x47, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x37, 0xD7 } },
        { { 0x83, 0x0B, 0x48, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x02, 0x46 } },
        { { 0x83, 0x0C, 0x49, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x01, 0xB0 } },
        { { 0x83, 0x0D, 0x4A, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x22, 0xEF } },
        { { 0x83, 0x0E, 0x4B, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xBA, 0xAB } },
        { { 0x83, 0x0F, 0x4C, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x64, 0x51 } },
        { { 0x83, 0x10, 0x4D, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x0E, 0x68 } },
        { { 0x83, 0x11, 0x4E, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x2D, 0x37 } },
        { { 0x83, 0x12, 0x4F, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xB5, 0x73 } },
        { { 0x83, 0x13, 0x50, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x46, 0x15 } },
        { { 0x83, 0x14, 0x51, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x45, 0xE3 } },
        { { 0x83, 0x15, 0x52, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0x66, 0xBC } },
        { { 0x83, 0x16, 0x53, 0x0A }, {    'l', '\x00',    'J',    'o',    'h',    'n',    ' ',    'P',    'o',    'w',    'e',    'l' }, { 0xFE, 0xF8 } },
        { { 0x83, 0x17, 0x54, 0x0A }, {    'l', '\x00',    'J',   0xF3,    'n',    's',    'i', '\x00',    'J',    'o',    'h',    'n' }, { 0x24, 0xF3 } },
        { { 0x83, 0x19, 0x55, 0x04 }, {    ' ',    'P',    'o',    'w',    'e',    'l',    'l', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0xBE, 0xC3 } },
        { { 0x87, 0x00, 0x56, 0x00 }, { '\x00', '\x01',    'S',    'o',    'u',    'n',    'd',    't',    'r',    'a',    'c',    'k' }, { 0xA2, 0xAE } },
        { { 0x87, 0x00, 0x57, 0x0A }, { '\x00', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x5B, 0x8A } },
        { { 0x8F, 0x00, 0x58, 0x00 }, { '\x00', '\x01', '\x19', '\x00', '\x24', '\x19', '\x00', '\x19', '\x00', '\x00', '\x00', '\x02' }, { 0x05, 0xB0 } },
        { { 0x8F, 0x01, 0x59, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x5A', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
        { { 0x8F, 0x02, 0x5A, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
};
// @formatter:on
// endregion

namespace {
    void check_track_text_data(const track_text_data& expected, const track_text_data& actual) {
        CHECK(expected.title == actual.title);
        CHECK(expected.performer == actual.performer);
        CHECK(expected.song_writer == actual.song_writer);
        CHECK(expected.composer == actual.composer);
        CHECK(expected.arranger == actual.arranger);
        CHECK(expected.trackNumber == actual.trackNumber);
    }

    void check_process_text_packs(const text_pack* textPacks, const std::vector<track_text_data>&& expected) {
        int numberOfCalls = 0;
        int expectedNumberOfCalls = expected.size();
        process_text_packs(textPacks, [&](const track_text_data& trackTextData) {
            if (numberOfCalls < expectedNumberOfCalls) {
                check_track_text_data(expected[numberOfCalls], trackTextData);
            }
            ++numberOfCalls;
        });
        CHECK(expectedNumberOfCalls == numberOfCalls);
    }
}

TEST_CASE("can read a cd with only an 11-character title and an 11-character track name") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',   '\0' }, { 0x20, 0x4F } },
            { { 0x80, 0x01, 0x01, 0x00 }, {    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o',    'm',    'e',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x02', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You", "", "", "", "", std::nullopt },
            { "I'm Awesome", "", "", "", "", 1 }
    });
}

TEST_CASE("can read a cd with titles long enough to span multiple packs") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',    ' ' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x01, 0x0C }, {    'o',    'n',    ' ',    'T',    'h',    'i',    's',    ' ',    'F',    'i',    'n',    'e' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x02, 0x0C }, {    ',',    ' ',    'S',    'u',    'n',    'n',    'y',    ' ',    'D',    'a',    'y',   '\0' }, { 0x20, 0x4F } },
            { { 0x80, 0x01, 0x03, 0x00 }, {    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o',    'm',    'e',    '!' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x04, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x04', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x05, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x06', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x06, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You on This Fine, Sunny Day", "", "", "", "", std::nullopt },
            { "I'm Awesome!", "", "", "", "", 1 }
    });
}

TEST_CASE("can read a cd with titles that share packs") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',    ' ' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x01, 0x0C }, {    'o',    'n',    ' ',    'T',    'h',    'i',    's',    ' ',    'B',    'r',    'i',    'g' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x02, 0x0C }, {    'h',    't',    ' ',    'A',    'f',    't',    'e',    'r',    'n',    'o',    'o',    'n' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x03, 0x0C }, {    '?',   '\0',    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o',    'm' }, { 0x20, 0x4F } },
            { { 0x80, 0x01, 0x04, 0x0A }, {    'e',    '!',   '\0',    'I',    ' ',    'A',    'm',    ' ',    'O',    'K',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x05, 0x00 }, { '\x01', '\x01', '\x02', '\x00', '\x04', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x06, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x07', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x07, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You on This Bright Afternoon?", "", "", "", "", std::nullopt },
            { "I'm Awesome!",                          "", "", "", "",            1 },
            { "I Am OK",                               "", "", "", "",            2 }
    });
}

TEST_CASE("can read a cd with titles that are empty") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',    ' ' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x01, 0x0C }, {    'o',    'n',    ' ',    'T',    'h',    'i',    's',    ' ',    'B',    'r',    'i',    'g' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x02, 0x0C }, {    'h',    't',    ' ',    'A',    'f',    't',    'e',    'r',    'n',    'o',    'o',    'n' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x03, 0x0C }, {    '?',   '\0',   '\0',    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o' }, { 0x20, 0x4F } },
            { { 0x80, 0x02, 0x04, 0x09 }, {    'm',    'e',    '!',   '\0',   '\0',   '\0',   '\0',   '\0',   '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x05, 0x00 }, { '\x01', '\x01', '\x02', '\x00', '\x04', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x06, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x07', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x07, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You on This Bright Afternoon?", "", "", "", "", std::nullopt },
            { "",                                      "", "", "", "",            1 },
            { "I'm Awesome!",                          "", "", "", "",            2 }
    });
}

TEST_CASE("can read a cd with only an 12-character title and an 12-character track name") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',    '?' }, { 0x20, 0x4F } },
            { { 0x80, 0x01, 0x01, 0x00 }, {    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o',    'm',    'e',    '!' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x02', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You?", "", "", "", "", std::nullopt },
            { "I'm Awesome!", "", "", "", "", 1 }
    });
}

TEST_CASE("can read a cd with only one track with only performers listed with one pack each") {
    text_pack textPacks[] = {
            { { 0x81, 0x00, 0x00, 0x00 }, {    'M',    'i',    'c',    'h',    'a',    'e',    'l',    ' ',    'M',    'i',    'l',    'l' }, { 0x20, 0x4F } },
            { { 0x81, 0x01, 0x01, 0x00 }, {    'J',   'o',    'h',    'n',    ' ',    'D',    'o',    'e',    '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x00', '\x02', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "", "Michael Mill", "", "", "", std::nullopt },
            { "", "John Doe", "", "", "", 1 }
    });
}

TEST_CASE("can read a cd-text with both titles and performers") {
    text_pack textPacks[] = {
            { { 0x80, 0x00, 0x00, 0x00 }, {    'H',    'o',    'w',    ' ',    'A',    'r',    'e',    ' ',    'Y',    'o',    'u',    ' ' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x01, 0x00 }, {    'o',    'n',    ' ',    'T',    'h',    'i',    's',    ' ',    'F',    'i',    'n',    'e' }, { 0x20, 0x4F } },
            { { 0x80, 0x00, 0x02, 0x00 }, {    ',',    ' ',    'S',    'u',    'n',    'n',    'y',    ' ',    'D',    'a',    'y',   '\0' }, { 0x20, 0x4F } },
            { { 0x80, 0x01, 0x03, 0x00 }, {    'I',   '\'',    'm',    ' ',    'A',    'w',    'e',    's',    'o',    'm',    'e',    '!' }, { 0x20, 0x4F } },
            { { 0x81, 0x00, 0x04, 0x00 }, {    'M',    'i',    'c',    'h',    'a',    'e',    'l',    ' ',    'M',    'i',    'l',    'l' }, { 0x20, 0x4F } },
            { { 0x81, 0x01, 0x05, 0x00 }, {    'J',   'o',    'h',    'n',    ' ',    'D',    'o',    'e',    '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x06, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x04', '\x02', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x07, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x08', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x08, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "How Are You on This Fine, Sunny Day", "Michael Mill", "", "", "", std::nullopt },
            { "I'm Awesome!", "John Doe", "", "", "", 1 }
    });
}

TEST_CASE("can read a cd with only one track with only song writers listed with one pack each") {
    text_pack textPacks[] = {
            { { 0x82, 0x00, 0x00, 0x00 }, {    'M',    'i',    'c',    'h',    'a',    'e',    'l',    ' ',    'M',    'i',    'l',    'l' }, { 0x20, 0x4F } },
            { { 0x82, 0x01, 0x01, 0x00 }, {    'J',   'o',    'h',    'n',    ' ',    'D',    'o',    'e',    '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x00', '\x00', '\x02', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "", "", "Michael Mill", "", "", std::nullopt },
            { "", "", "John Doe", "", "", 1 }
    });
}

TEST_CASE("can read a cd with only one track with only composers listed with one pack each") {
    text_pack textPacks[] = {
            { { 0x83, 0x00, 0x00, 0x00 }, {    'M',    'i',    'c',    'h',    'a',    'e',    'l',    ' ',    'M',    'i',    'l',    'l' }, { 0x20, 0x4F } },
            { { 0x83, 0x01, 0x01, 0x00 }, {    'J',   'o',    'h',    'n',    ' ',    'D',    'o',    'e',    '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x00', '\x00', '\x00', '\x02', '\x00', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "", "", "", "Michael Mill", "", std::nullopt },
            { "", "", "", "John Doe", "", 1 }
    });
}

TEST_CASE("can read a cd with only one track with only arrangers listed with one pack each") {
    text_pack textPacks[] = {
            { { 0x84, 0x00, 0x00, 0x00 }, {    'M',    'i',    'c',    'h',    'a',    'e',    'l',    ' ',    'M',    'i',    'l',    'l' }, { 0x20, 0x4F } },
            { { 0x84, 0x01, 0x01, 0x00 }, {    'J',   'o',    'h',    'n',    ' ',    'D',    'o',    'e',    '\0',   '\0',   '\0',   '\0' }, { 0x20, 0x4F } },
            { { 0x8F, 0x00, 0x02, 0x00 }, { '\x01', '\x01', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x02', '\x00', '\x00', '\x00' }, { 0x05, 0xB0 } },
            { { 0x8F, 0x01, 0x03, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x03', '\x04', '\x00', '\x00', '\x00' }, { 0xAE, 0x68 } },
            { { 0x8F, 0x02, 0x04, 0x00 }, { '\x00', '\x00', '\x00', '\x00', '\x09', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00' }, { 0x6A, 0xA5 } }
    };

    check_process_text_packs(textPacks, {
            { "", "", "", "", "Michael Mill", std::nullopt },
            { "", "", "", "", "John Doe", 1 }
    });
}