//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <algorithm>
#include <deque>
#include <memory>
#include <catch2/catch.hpp>

#include "core/scsi-cd-data-iterator.hpp"

using music_library::core::scsi_cd_data_iterator;

template <typename Iter>
typename std::iterator_traits<Iter>::difference_type count(Iter begin, Iter end) {
    return std::count_if(begin, end, [](auto value) { return true; });
}

TEST_CASE("A cd data iterator can be empty") {
    class empty_sector_reader {
    public:
        bool empty() const { return true; }
        void pop_front() {}

        const std::vector<std::uint8_t>& front() const { return temp; }
    private:
        std::vector<uint8_t> temp;
    };

    empty_sector_reader sectorReader;
    scsi_cd_data_iterator<empty_sector_reader> begin(sectorReader);
    scsi_cd_data_iterator<empty_sector_reader> end;

    CHECK(0 == count(begin, end));
}

TEST_CASE("A cd data iterator with chunk size of 1 sector and sector size of 1 byte can have values") {
    class single_byte_sector_reader {
    public:
        single_byte_sector_reader(std::initializer_list<uint8_t> values): m_data(), m_current(1) {
            std::copy(values.begin(), values.end(), std::back_inserter(m_data));
            m_current[0] = m_data.front();
        }

        bool empty() const { return m_data.empty(); }

        void pop_front() {
            m_data.pop_front();
            if (!m_data.empty()) {
                m_current[0] = m_data.front();
            }
        }

        const std::vector<std::uint8_t>& front() const {
            return m_current;
        }
    private:
        std::deque<uint8_t> m_data;
        std::vector<uint8_t> m_current;
    };

    typedef scsi_cd_data_iterator<single_byte_sector_reader> single_byte_iterator;

    single_byte_sector_reader countReader = single_byte_sector_reader({ 12, 7, 32 });
    single_byte_iterator begin(countReader);
    single_byte_iterator end;

    CHECK(3 == count(begin, end));

    single_byte_sector_reader sumReader = single_byte_sector_reader({ 12, 7, 32 });
    begin = single_byte_iterator(sumReader);
    std::vector<uint8_t> results;
    std::copy(begin, end, std::back_inserter(results));
    CHECK(std::vector<uint8_t>({12, 7, 32}) == results);
}

TEST_CASE("A cd data iterator with chunk size of 1 sector and sector size of 5 byte can have values") {
    class multi_byte_sector_reader {
    public:
        multi_byte_sector_reader(std::initializer_list<std::initializer_list<uint8_t>> values) {
            std::copy(values.begin(), values.end(), std::back_inserter(m_data));
        }

        bool empty() const { return m_data.empty(); }

        void pop_front() {
            m_data.pop_front();
        }

        const std::vector<std::uint8_t>& front() const {
            return m_data.front();
        }
    private:
        std::deque<std::vector<uint8_t>> m_data;
    };

    typedef scsi_cd_data_iterator<multi_byte_sector_reader> multi_byte_iterator;

    multi_byte_sector_reader sectorReader = { { 12, 7, 32, 19, 18 }, { 99, 109, 237, 24, 25 } };
    multi_byte_iterator begin(sectorReader);
    multi_byte_iterator end;

    std::vector<uint8_t> results;
    std::copy(begin, end, std::back_inserter(results));
    CHECK(std::vector<uint8_t>({ 12, 7, 32, 19, 18, 99, 109, 237, 24, 25 }) == results);
}