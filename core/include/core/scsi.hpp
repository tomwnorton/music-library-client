//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_HPP

namespace music_library::core {
    class cd_drive_info {
    public:
        cd_drive_info(music_library::core::unicode_string drive,
                music_library::core::unicode_string vendor,
                music_library::core::unicode_string productNumber)
                : drive(std::move(drive)), vendor(std::move(vendor)), productNumber(std::move(productNumber)) {}

        const music_library::core::unicode_string& get_drive() const { return drive; }
        const music_library::core::unicode_string& get_vendor() const { return vendor; }
        const music_library::core::unicode_string& get_product_number() const { return productNumber; }

    private:
        music_library::core::unicode_string drive;
        music_library::core::unicode_string vendor;
        music_library::core::unicode_string productNumber;
    };

    class track_length {
    public:
        explicit track_length(std::uint32_t sectors) : m_sectors(sectors) {}

        unsigned int get_minutes() const { return m_sectors / 75 / 60; }
        unsigned int get_seconds() const { return m_sectors / 75 % 60; }
        unsigned int get_frames() const { return m_sectors % 75; }
    private:
        std::uint32_t m_sectors;
    };

    struct text_info {
        music_library::core::unicode_string title;
        music_library::core::unicode_string performer;
        music_library::core::unicode_string song_writer;
        music_library::core::unicode_string composer;
        music_library::core::unicode_string arranger;
    };

    struct track_info {
        text_info text;
        track_length length;
        std::uint32_t beginSector;
    };

    struct disc_info {
        text_info album_info;
        std::vector<track_info> tracks;
    };

    class scsi_service {
    public:
        std::vector<cd_drive_info> get_cd_drives() const;
        disc_info get_tracks(const unicode_string& drivePath) const;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SCSI_HPP
