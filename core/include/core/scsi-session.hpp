//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_SESSION_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_SESSION_HPP

namespace music_library::core {
    struct io_control_handle;
    class scsi_session {
    public:
        scsi_session(const unicode_string& devicePath);

        scsi_session(const scsi_session&) = delete;

        scsi_session(scsi_session&& rhs) noexcept;

        ~scsi_session();

        scsi_session& operator=(const scsi_session&) = delete;

        scsi_session& operator=(scsi_session&& rhs) noexcept;

        void run_command(const unsigned char* request, unsigned char requestSize, void* responseBuffer, unsigned int responseBufferSize) const;
    private:
        io_control_handle* m_handle;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SESSION_HPP
