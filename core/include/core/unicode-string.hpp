//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_UNICODE_STRING_HPP
#define MUSIC_LIBRARY_CLIENT_UNICODE_STRING_HPP

namespace music_library::core {
    class unicode_string {
    public:
        unicode_string();
        unicode_string(const char32_t* cstring);
        unicode_string(const char32_t* cstring, std::size_t lengthInCharacters);
        unicode_string(const char16_t* cstring);
        unicode_string(const char16_t* cstring, std::size_t lengthInCharacters);
        unicode_string(const char* cstring);
        unicode_string(const char* cstring, std::size_t lengthInCharacters);
        unicode_string(const unicode_string& rhs);
        unicode_string(unicode_string&& rhs) noexcept;
        ~unicode_string();

        static unicode_string from_iso_8859_1(const char* const isoString);

        std::size_t length() const;

        unicode_string left_trim() const;
        unicode_string right_trim() const;
        unicode_string trim() const;

        std::u32string to_utf32_string() const;
        std::u16string to_utf16_string() const;
        std::string to_utf8_string() const;

        unicode_string& operator=(const unicode_string& rhs);
        unicode_string& operator+=(const unicode_string& rhs);

        friend bool operator==(const unicode_string& lhs, const unicode_string& rhs);
        friend bool operator!=(const unicode_string& lhs, const unicode_string& rhs);
        friend bool operator<(const unicode_string& lhs, const unicode_string& rhs);
        friend bool operator>(const unicode_string& lhs, const unicode_string& rhs);
        friend bool operator<=(const unicode_string& lhs, const unicode_string& rhs);
        friend bool operator>=(const unicode_string& lhs, const unicode_string& rhs);

        friend unicode_string operator+(const unicode_string& lhs, const unicode_string& rhs);
    private:

        std::size_t m_length;
        char32_t* m_data;
    };

    namespace unicode_string_literals {
        unicode_string operator "" _us(const char*, std::size_t);
        unicode_string operator "" _us(const char16_t*, std::size_t);
        unicode_string operator "" _us(const char32_t*, std::size_t);
    }
}

#endif //MUSIC_LIBRARY_CLIENT_UNICODE_STRING_HPP
