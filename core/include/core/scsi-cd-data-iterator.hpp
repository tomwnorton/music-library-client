//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_CD_DATA_ITERATOR_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_CD_DATA_ITERATOR_HPP

namespace music_library::core {
    template <class SectorReader>
    class scsi_cd_data_iterator {
    public:
        typedef const std::uint8_t value_type;
        typedef const std::uint8_t& reference;
        typedef const void pointer;
        typedef std::ptrdiff_t difference_type;
        typedef std::input_iterator_tag iterator_category;

        scsi_cd_data_iterator() : m_sectorReader(nullptr), m_position(nullptr) {}

        scsi_cd_data_iterator(SectorReader& sectorReader) : m_sectorReader(&sectorReader), m_position(new position()) {
            if (!m_sectorReader->empty()) {
                m_position->set_sector(m_sectorReader->front());
            }
        }

        scsi_cd_data_iterator(const scsi_cd_data_iterator<SectorReader>& rhs) {
            m_sectorReader = rhs.m_sectorReader;
            m_position = rhs.m_position;
        }

        scsi_cd_data_iterator(scsi_cd_data_iterator<SectorReader>&& rhs) {
            m_sectorReader = rhs.m_sectorReader;
            m_position = rhs.m_position;
        }

        scsi_cd_data_iterator<SectorReader>& operator=(const scsi_cd_data_iterator<SectorReader>& rhs) {
            if (this == &rhs) return *this;

            m_sectorReader = rhs.m_sectorReader;
            m_position = rhs.m_position;
            return *this;
        }

        scsi_cd_data_iterator<SectorReader>& operator=(scsi_cd_data_iterator<SectorReader>&& rhs) {
            if (this == &rhs) return *this;

            m_sectorReader = rhs.m_sectorReader;
            m_position = rhs.m_position;
            return *this;
        }

        reference operator*() { return m_position->get_value(); }

        scsi_cd_data_iterator<SectorReader>& operator++() {
            m_position->move_next();
            if (m_position->empty()) {
                m_sectorReader->pop_front();
                if (!m_sectorReader->empty()) {
                    m_position->set_sector(m_sectorReader->front());
                }
            }
            return *this;
        }

        bool operator==(const scsi_cd_data_iterator<SectorReader>& rhs) {
            bool result = m_sectorReader == rhs.m_sectorReader && m_position == rhs.m_position
                          || m_sectorReader != nullptr && m_sectorReader->empty() && rhs.m_sectorReader == nullptr
                          || rhs.m_sectorReader != nullptr && rhs.m_sectorReader->empty() && m_sectorReader == nullptr;
            return result;
        }

        bool operator!=(const scsi_cd_data_iterator<SectorReader>& rhs) { return !(*this == rhs); }
    private:
        class position {
        public:
            void set_sector(std::vector<std::uint8_t> sector) {
                m_currentSector = sector;
                m_iterator = m_currentSector.begin();
            }

            bool empty() const { return m_currentSector.end() == m_iterator; }

            void move_next() { ++m_iterator; }

            const std::uint8_t& get_value() { return *m_iterator; }
        private:
            std::vector<std::uint8_t> m_currentSector;
            std::vector<std::uint8_t>::iterator m_iterator;
        };
        SectorReader* m_sectorReader;
        std::shared_ptr<position> m_position;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SCSI_CD_DATA_ITERATOR_HPP
