//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_COMMAND_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_COMMAND_HPP

namespace music_library::core {
    template<typename T>
    struct scsi_request_traits {
        static const size_t OFFSET = 0UL;
    };

    template<std::uint8_t COMMAND_CODE, typename Request, typename Response>
    class scsi_command {
    public:
        scsi_command() {
            std::memset(&m_request, 0, sizeof(Request));
            m_request.operation_code = COMMAND_CODE;
        }

        Request& request() {
            return m_request;
        }

        template<typename Consumer>
        void execute(const scsi_session& session, Consumer consumer, unsigned int responseSize = sizeof(Response)) const {
            std::vector<std::uint8_t> buffer(responseSize);
            std::fill(buffer.begin(), buffer.end(), 0);
            size_t requestOffset = scsi_request_traits<Request>::OFFSET;
            session.run_command(reinterpret_cast<const unsigned char*>(&m_request) + requestOffset, sizeof(Request) - requestOffset, buffer.data(), buffer.size());
            auto response = reinterpret_cast<Response*>(buffer.data());
            consumer(*response);
        }

    private:
        Request m_request;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SCSI_COMMAND_HPP
