//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_READ_CD_DATA_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_READ_CD_DATA_HPP

namespace music_library::core {
    const int BYTES_PER_SECTOR = 2'048;

    class scsi_sector_reader;
    struct scsi_sector_reader_factory_impl;
    class scsi_sector_reader_factory {
    public:
        scsi_sector_reader_factory(const unicode_string& drivePath);
        scsi_sector_reader_factory(scsi_sector_reader_factory&& rhs);
        scsi_sector_reader_factory(const scsi_sector_reader_factory&) = delete;
        ~scsi_sector_reader_factory();

        scsi_sector_reader_factory& operator=(scsi_sector_reader_factory&& rhs);
        scsi_sector_reader_factory& operator=(const scsi_sector_reader_factory&) = delete;

        std::unique_ptr<scsi_sector_reader> create_reader(int startSector, int endSector);
    private:
        scsi_sector_reader_factory_impl* m_impl;
    };

    struct scsi_sector_reader_impl;
    class scsi_sector_reader {
        friend class scsi_sector_reader_factory;
    public:
        ~scsi_sector_reader();

        bool empty() const;
        void pop_front();
        const std::vector<std::uint8_t>& front() const;
    private:
        scsi_sector_reader(scsi_sector_reader_impl* impl);
        scsi_sector_reader_impl* m_impl;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SCSI_READ_CD_DATA_HPP
