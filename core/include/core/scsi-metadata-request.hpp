//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_SCSI_METADATA_REQUEST_HPP
#define MUSIC_LIBRARY_CLIENT_SCSI_METADATA_REQUEST_HPP

namespace music_library::core {
    struct read_toc_pma_atip_request {
        uint8_t first_reserved_block;
        uint8_t operation_code;
        uint8_t second_reserved_block : 1;
        bool msf : 1;
        uint8_t third_reserved_block : 6;
        uint8_t format : 4; /// 0 means Table of Contents, 5 means CD Text
        uint8_t fourth_reserved_block: 4;
        uint8_t fifth_reserved_block;
        uint8_t sixth_reserved_block;
        uint8_t seventh_reserved_block;
        union {
            uint8_t track_number;
            uint8_t session_number;
        };
        big_endian<2UL> allocation_length;
        uint8_t control;
    };

    template<>
    struct scsi_request_traits<read_toc_pma_atip_request> {
        static const size_t OFFSET = 1UL;
    };
}

#endif //MUSIC_LIBRARY_CLIENT_SCSI_METADATA_REQUEST_HPP
