//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_BIG_ENDIAN_HPP
#define MUSIC_LIBRARY_CLIENT_BIG_ENDIAN_HPP

namespace music_library::core {
    template <size_t SIZE_IN_BYTES>
    struct is_valid_type_size {
        static const bool value = SIZE_IN_BYTES <= 8UL && SIZE_IN_BYTES > 0UL;
    };

    template <size_t SIZE_IN_BYTES>
    struct size_to_integer;

    template <>
    struct size_to_integer<1UL> {
        typedef uint8_t value_type;
    };

    template <>
    struct size_to_integer<2UL> {
        typedef uint16_t value_type;
    };

    template <>
    struct size_to_integer<3UL> {
        typedef uint32_t value_type;
    };

    template <>
    struct size_to_integer<4UL> {
        typedef uint32_t value_type;
    };

    template <>
    struct size_to_integer<5UL> {
        typedef uint64_t value_type;
    };

    template <>
    struct size_to_integer<6UL> {
        typedef uint64_t value_type;
    };

    template <>
    struct size_to_integer<7UL> {
        typedef uint64_t value_type;
    };

    template <>
    struct size_to_integer<8UL> {
        typedef uint64_t value_type;
    };

    template<size_t SIZE_IN_BYTES, bool VALID = is_valid_type_size<SIZE_IN_BYTES>::value>
    class big_endian;

    template<size_t SIZE_IN_BYTES>
    class big_endian<SIZE_IN_BYTES, true> {
    public:
        typedef typename size_to_integer<SIZE_IN_BYTES>::value_type value_type;

        big_endian();
        big_endian(value_type value);

        operator value_type() const;
    private:
        uint8_t big_endian_value[SIZE_IN_BYTES];
    };

    template <size_t SIZE_IN_BYTES>
    big_endian<SIZE_IN_BYTES, true>::big_endian() {
        for (auto i = 0UL; i < SIZE_IN_BYTES; ++i) {
            big_endian_value[i] = 0U;
        }
    }

    template <size_t SIZE_IN_BYTES>
    big_endian<SIZE_IN_BYTES, true>::big_endian(typename big_endian<SIZE_IN_BYTES, true>::value_type value) {
        for (auto index = 0UL; index < SIZE_IN_BYTES; ++index) {
            auto bitsToShift = (SIZE_IN_BYTES - index - 1UL) * 8UL;
            big_endian_value[index] = static_cast<uint8_t>(value >> bitsToShift);
        }
    }

    template <size_t SIZE_IN_BYTES>
    big_endian<SIZE_IN_BYTES, true>::operator value_type() const {
        value_type result = 0UL;
        for (auto index = 0UL; index < SIZE_IN_BYTES; ++index) {
            auto bitsToShift = (SIZE_IN_BYTES - index - 1UL) * 8UL;
            result |= big_endian_value[index] << bitsToShift;
        }
        return result;
    }
}

#endif //MUSIC_LIBRARY_CLIENT_BIG_ENDIAN_HPP
