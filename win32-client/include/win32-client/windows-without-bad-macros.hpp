//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef MUSIC_LIBRARY_CLIENT_WINDOWS_WITHOUT_BAD_MACROS_HPP
#define MUSIC_LIBRARY_CLIENT_WINDOWS_WITHOUT_BAD_MACROS_HPP

#include <windows.h>

// Win32 defines min(a, b) and max(a, b) as fucking macros that completely replace the min() and max() functions on
// std::numeric_limits
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

#endif //MUSIC_LIBRARY_CLIENT_WINDOWS_WITHOUT_BAD_MACROS_HPP
