//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Visual Studio forces its own manifest file to be embedded into the executable.  Normally, you need to supply your own
// mainfest file in order to get the modern-looking controls instead of the Windows 9x-style controls.  However, since
// visual studio keeps overwriting our manifest file, we need to use this line to get the newer controls.
#pragma comment(linker, "\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#include <string>
#include <stdexcept>
#include "win32-client/windows-without-bad-macros.hpp"
#include <commctrl.h>
#include "win32-client/main-window.hpp"
#include "core/unicode-string.hpp"

void report_error(const char* message) {
    music_library::core::unicode_string text(message);
    auto utf16 = text.to_utf16_string();
    auto wide_string = reinterpret_cast<LPCWSTR>(utf16.c_str());
    MessageBox(nullptr, wide_string, L"Error", MB_OK | MB_ICONERROR);
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {
    static_assert(sizeof(size_t) == 8, "64-bit compiler is required");
	INITCOMMONCONTROLSEX init_common_controls_ex;
	init_common_controls_ex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	init_common_controls_ex.dwICC = ICC_STANDARD_CLASSES;
	if (!InitCommonControlsEx(&init_common_controls_ex)) {
		MessageBox(nullptr, L"Could not load common controls!", L"Music Library Client", MB_OK | MB_ICONERROR); // NOLINT(hicpp-signed-bitwise)
		return -1;
	}
	try {
        music_library::win32::client::main_window main_window(hInstance, nCmdShow);

        MSG msg;
        while (GetMessage(&msg, nullptr, 0, 0) > 0) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        return 0;
    } catch (const std::exception& e) {
	    report_error(e.what());
	    return -1;
	}
}

#if defined(____MINGW64__) || defined(__MINGW32__)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR ignored, int nCmdShow) {
    return wWinMain(hInstance, hPrevInstance, GetCommandLine(), nCmdShow);
}
#endif