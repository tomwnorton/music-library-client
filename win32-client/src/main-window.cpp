//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <stdexcept>
#include "win32-client/windows-without-bad-macros.hpp"
#include "win32-client/main-window.hpp"

namespace music_library::win32::client {
    namespace {
        LPCWSTR MAIN_WINDOW_CLASS_NAME = L"io.bitbucket.tomwnorton.music-library-client.main-window";
        LPCWSTR MAIN_WINDOW_TITLE = L"Main Window";
        DWORD EXTENDED_WINDOW_STYLE = WS_EX_CLIENTEDGE;
        DWORD WINDOW_STYLE = WS_OVERLAPPEDWINDOW;
        int USE_DEFAULT_X_POS = CW_USEDEFAULT;
        int USE_DEFAULT_Y_POS = CW_USEDEFAULT;
        int INITIAL_WIDTH = 1280;
        int INITIAL_HEIGHT = 720;
        HWND NO_PARENT_WINDOW = nullptr;
        HMENU NO_MENU = nullptr;
        LPVOID NO_WINDOW_SPECIFIC_DATA = nullptr;

        LRESULT CALLBACK main_window_callback(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
            switch (msg) {
                case WM_CLOSE:
                    if (MessageBox(hWnd, L"Close the window?", L"Music Library", MB_YESNO | MB_ICONQUESTION) == IDYES) {
                        DestroyWindow(hWnd);
                    }
                    break;
                case WM_DESTROY:
                    PostQuitMessage(0);
                    break;
                default:
                    return DefWindowProc(hWnd, msg, wParam, lParam);
            }
            return 0;
        }
    }

    main_window::main_window(HINSTANCE hInstance, int nCmdShow) {
        WNDCLASSEXW wc;
        wc.cbSize = sizeof(WNDCLASSEXW);
        wc.style = 0;
        wc.cbClsExtra = 0;
        wc.cbWndExtra = 0;
        wc.hInstance = 0;
        wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
        wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
        wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
        wc.lpszMenuName = nullptr;
        wc.lpszClassName = MAIN_WINDOW_CLASS_NAME;
        wc.lpfnWndProc = main_window_callback;
        if (!RegisterClassEx(&wc)) {
            throw std::runtime_error("Could not register main window class");
        }
        m_handle = CreateWindowEx(
                EXTENDED_WINDOW_STYLE,
                MAIN_WINDOW_CLASS_NAME,
                MAIN_WINDOW_TITLE,
                WINDOW_STYLE,
                USE_DEFAULT_X_POS,
                USE_DEFAULT_Y_POS,
                INITIAL_WIDTH,
                INITIAL_HEIGHT,
                NO_PARENT_WINDOW,
                NO_MENU,
                hInstance,
                NO_WINDOW_SPECIFIC_DATA);
        if (m_handle == nullptr) {
            throw std::runtime_error("Could not create main window");
        }

        ShowWindow(m_handle, nCmdShow);
        UpdateWindow(m_handle);
    }
}