//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdio>
#include <vector>
#include <string>
#include "core/unicode-string.hpp"
#include "core/exception.hpp"
#include "core/scsi.hpp"

using namespace music_library::core;
using std::printf;

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Expected CD path in argument");
        return -1;
    }
    try {
        scsi_service scsiService;
        auto albumInfo = scsiService.get_tracks(argv[1]);
        printf("Index Title                                                        Start Sector Time    \n");
        int index = 0;
        printf("%5d %-60s %12c %8c\n", 0, albumInfo.album_info.title.to_utf8_string().c_str(), ' ', ' ');
        for (auto& track : albumInfo.tracks) {
            printf("%5d %-60s %12X %02d:%02d.%02d\n",
                   ++index,
                   track.text.title.to_utf8_string().c_str(),
                   track.beginSector,
                   track.length.get_minutes(),
                   track.length.get_seconds(),
                   track.length.get_frames());
        }
        return 0;
    } catch (const exception& e) {
        std::fprintf(stderr, "An error occurred: %s\n", e.get_message().to_utf8_string().c_str());
        return -1;
    }
}