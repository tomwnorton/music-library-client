//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cstdint>
#include <cstring>

#include <iostream>
#include <iomanip>
#include <iterator>
#include <memory>
#include <sstream>
#include <vector>

#include "core/unicode-string.hpp"
#include "core/exception.hpp"
#include "core/scsi-read-cd-data.hpp"
#include "core/scsi-cd-data-iterator.hpp"

using namespace music_library::core;

int to_hex(const char* value) {
   int result;
   std::istringstream in(value);
   in >> std::hex >> result;
   return result;
}

/**
 * Output iterator to copy a value to stdout while updating a status line on stderr
 *
 * I'm using the standard C I/O instead of standard C++ I/O because iostream is a pain in the
 * ass to use, especially when it comes to manipulators.
 */
class status_line_output_iterator {
public:
    typedef void value_type;
    typedef void difference_type;
    typedef void reference;
    typedef void pointer;
    typedef std::output_iterator_tag iterator_category;

    status_line_output_iterator(size_t expectedLength) : m_expectedLength(expectedLength), m_actualLength(0UL) {}

    status_line_output_iterator& operator*() { return *this; }      /// No-op
    status_line_output_iterator& operator++() { return *this; }     /// No-op

    status_line_output_iterator& operator=(uint8_t value) {
        putchar(value);
        ++m_actualLength;
        fprintf(stderr, "%9lu / %9lu\r", m_actualLength, m_expectedLength);
        return *this;
    }
private:
    size_t m_expectedLength;
    size_t m_actualLength;
};

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "Expected the following arguments <disc-path> <start-index> <end-index>" << std::endl;
        return -1;
    }

    unicode_string discPath = argv[1];
    auto startIndex = to_hex(argv[2]);
    auto endIndex = to_hex(argv[3]);
    auto expectedLength = (endIndex - startIndex) * 2'048UL;

    try {
        scsi_sector_reader_factory factory(discPath);
        auto reader = factory.create_reader(startIndex, endIndex);
        scsi_cd_data_iterator<scsi_sector_reader> begin(*reader);
        scsi_cd_data_iterator<scsi_sector_reader> end;
        std::copy(begin, end, status_line_output_iterator(expectedLength));
        std::fprintf(stderr, "\n");
    } catch (const exception& e) {
        std::cerr << "Failed due to: " << e.get_message().to_utf8_string() << std::endl;
        return -1;
    } catch (...) {
        std::cerr << "An unknown error occurred\n" << std::endl;
    }

    return 0;
}