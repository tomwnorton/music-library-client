//  Client for my music-library web-app
//  Copyright (C) 2019  Thomas Norton
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#include <cstdio>
#include <vector>
#include <string>
#include "core/unicode-string.hpp"
#include "core/scsi.hpp"

using namespace music_library::core;
using std::printf;

int main() {
    scsi_service scsiService;
    auto drives = scsiService.get_cd_drives();

    printf("Drive       Vendor   Product Number  \n");
    for (auto const& drive : drives) {
        printf("%-11s %-8s %-16s\n",
                drive.get_drive().to_utf8_string().c_str(),
                drive.get_vendor().to_utf8_string().c_str(),
                drive.get_product_number().to_utf8_string().c_str());
    }
    return 0;
}